<?php

return [
    "sms" => [
        "auth" => "Edutoday - Your login code: :code"
    ],
    "phone" => [
        "required" => "Phone number is a required field",
        "format" => "Invalid phone number format",
        "repeat_request" => "This phone number still has a code. A repeat code can be sent in 2 minutes",
    ],
    "code" => [
        "required" => "Code is a required parameter",
        "wrong" => "Wrong code",
    ]
];
