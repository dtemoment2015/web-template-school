<?php

return [
    "sms" => [
        "auth" => "Tasdiqlash kodingiz: :code"
    ],
    "phone" => [
        "required" => "Telefon raqami - majburiy maydon",
        "format" => "Telefon raqami formati noto'g'ri",
        "repeat_request" => "Bu telefon raqamida kod bor. Takroriy kod 2 daqiqada yuborilishi mumkin",
    ],
    "code" => [
        "required" => "Kod zarur parametr",
        "wrong" => "Noto'g'ri kod",
    ]
];
