<?php

namespace App\Observers;

use App\Models\Business;
use Illuminate\Support\Facades\Cache;

class BusinessObserver
{
    public function updated(Business $business)
    {
        foreach (config('translatable.locales') as $lang) {
            Cache::forget('business_' . $business->id . $lang);
            Cache::forget('business_languages' . $business->id);
            Cache::forget('business_domain_' . $business->name . '.edutoday.uz' . $lang);
        }

        foreach ($business->domains as $domain) {
            foreach (config('translatable.locales') as $lang) {
                Cache::forget('business_domain_' . $domain->name . $lang);
            }
        }
    }
}
