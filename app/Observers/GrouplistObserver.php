<?php

namespace App\Observers;

use App\Models\Business\Grouplist;

class GrouplistObserver
{
    /**
     * Handle the Grouplist "created" event.
     *
     * @param  \App\Models\Grouplist  $grouplist
     * @return void
     */
    public function created(Grouplist $grouplist)
    {
        //когда идет смена.ы
        $group = $grouplist->group()->firstOrFail();
        $group->grouplist()->associate($grouplist);
        $group->save();
    }

    /**
     * Handle the Grouplist "updated" event.
     *
     * @param  \App\Models\Grouplist  $grouplist
     * @return void
     */
    public function updated(Grouplist $grouplist)
    {
        //
    }

    /**
     * Handle the Grouplist "deleted" event.
     *
     * @param  \App\Models\Grouplist  $grouplist
     * @return void
     */
    public function deleted(Grouplist $grouplist)
    {
        //
    }

    /**
     * Handle the Grouplist "restored" event.
     *
     * @param  \App\Models\Grouplist  $grouplist
     * @return void
     */
    public function restored(Grouplist $grouplist)
    {
        //
    }

    /**
     * Handle the Grouplist "force deleted" event.
     *
     * @param  \App\Models\Grouplist  $grouplist
     * @return void
     */
    public function forceDeleted(Grouplist $grouplist)
    {
        //
    }
}
