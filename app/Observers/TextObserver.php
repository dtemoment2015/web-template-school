<?php

namespace App\Observers;

use App\Models\Project\Text;
use Illuminate\Support\Facades\Cache;

class TextObserver
{
    /**
     * Handle the Tex "created" event.
     *
     * @param  \App\Models\Tex  $text
     * @return void
     */
    public function created(Text $text)
    {
        foreach (config('translatable.locales') as $lang) {
            Cache::forget('text_' . $lang);
            Cache::forget('features_' . $lang);
            Cache::forget('localizations_' . $lang);
            Cache::forget('features_' . $lang);
        }
    }

    /**
     * Handle the Tex "updated" event.
     *
     * @param  \App\Models\Tex  $text
     * @return void
     */
    public function updated(Text $text)
    {
        foreach (config('translatable.locales') as $lang) {
            Cache::forget('text_' . $lang);
            Cache::forget('features_' . $lang);
            Cache::forget('localizations_' . $lang);
            Cache::forget('features_' . $lang);
        }
    }

    /**
     * Handle the Business "deleted" event.
     *
     * @param  \App\Models\Business  $business
     * @return void
     */
    public function deleted(Text $text)
    {
        foreach (config('translatable.locales') as $lang) {
            Cache::forget('text_' . $lang);
            Cache::forget('features_' . $lang);
            Cache::forget('localizations_' . $lang);
            Cache::forget('features_' . $lang);
        }
    }

    /**
     * Handle the Business "restored" event.
     *
     * @param  \App\Models\Business  $business
     * @return void
     */
    public function restored(Text $text)
    {
        foreach (config('translatable.locales') as $lang) {
            Cache::forget('text_' . $lang);
            Cache::forget('features_' . $lang);
            Cache::forget('localizations_' . $lang);
            Cache::forget('features_' . $lang);
        }
    }

    /**
     * Handle the Business "force deleted" event.
     *
     * @param  \App\Models\Business  $business
     * @return void
     */
    public function forceDeleted(Text $text)
    {
        foreach (config('translatable.locales') as $lang) {
            Cache::forget('text_' . $lang);
            Cache::forget('features_' . $lang);
            Cache::forget('localizations_' . $lang);
            Cache::forget('features_' . $lang);
        }
    }
}
