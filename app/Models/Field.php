<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Field extends Model  implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable =
    [
        'type',
        'code',
        'position',
        'is_required',
    ];

    public $translatedAttributes = [
        'label',
        'placeholder',
    ];

    protected $casts = [
        'is_required' => 'bool'
    ];
}
