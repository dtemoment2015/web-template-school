<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Form extends Model  implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable =
    [
        'code',
        'is_active',
    ];

    // protected $with =['fields'];

    protected $casts = [
        'is_active' => 'bool'
    ];
    
    public $translatedAttributes = [
        'title',
        'description',
    ];

    public function fields()
    {
        return $this->hasMany(Field::class);
    }
}
