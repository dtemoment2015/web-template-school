<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Location extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = [
        'title',
    ];

    protected $fillable = [
        'code'
    ];

    public function parent()
    {
        return $this->hasOne(
            self::class,
            'id',
            'parent_id'
        );
    }

    public function childs()
    {
        return $this->hasMany(
            self::class,
            'parent_id',
            'id'
        );
    }
}
