<?php

namespace App\Models\Project;

use App\Models\Business\Post;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Rubric extends Model implements TranslatableContract
{
    
    use HasFactory, Translatable;

    protected $fillable = [
        'code',
        'position',
    ];

    public $translatedAttributes = [
        'title',
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
