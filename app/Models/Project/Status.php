<?php

namespace App\Models\Project;

use App\Enums\StatusType;
use App\Models\Business\Post;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Status extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = [
        'code',
        'type',
    ];

    protected $casts = [
        'type' => StatusType::class
    ];

    public $translatedAttributes = [
        'title',
        'description',
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
