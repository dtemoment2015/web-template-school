<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Tariff extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = [
        'code',
        'is_active',
        'period',
        'currency',
        'price',
        'max_repeat',
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];

    public $translatedAttributes = [
        'title',
        'description',
    ];
}
