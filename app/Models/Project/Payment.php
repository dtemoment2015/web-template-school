<?php

namespace App\Models\Project;

use App\Casts\ImageCast;
use App\Enums\PaymentType;
use App\Traits\UploadModelImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Payment extends Model implements TranslatableContract
{
    use HasFactory, Translatable, UploadModelImage;

    protected $fillable = [
        'code',
        'logo',
        'currency',
        'is_active',
        'is_online',
        'type',
        'transaction_price',
        'transaction_price_percent',
    ];

    protected $casts = [
        'is_active' => 'boolean',
        'is_online' => 'boolean',
        'type' => PaymentType::class,
        'logo' =>  ImageCast::class
    ];

    public $translatedAttributes = [
        'title',
        'description',
    ];


    public function setLogoAttribute($val)
    {
        $key = 'logo';
        $this->attributes[$key] = $this->loadImage($val, $key, 200);
    }
}
