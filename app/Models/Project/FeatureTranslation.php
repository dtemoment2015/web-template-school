<?php

namespace App\Models\Project;

use App\Traits\MultiPrimaryKeyTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeatureTranslation extends Model
{
    use HasFactory, MultiPrimaryKeyTrait;

    public $timestamps = false;

    public $incrementing = false;

    protected $primaryKey = [
        'feature_id',
        'locale'
    ];

    protected $fillable = [
        'title',
        'description'
    ];

    // public function setTitleAttribute($val){       
    //     $this->attributes['title'] = $val;
    // }

}
