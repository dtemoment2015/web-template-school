<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Nicolaslopezj\Searchable\SearchableTrait;

class Text extends Model implements TranslatableContract
{

    use HasFactory, Translatable, SearchableTrait;

    protected $searchable = [
        'columns' => [
            'texts.key' => 10,
            'text_translations.value' => 5
        ],
        'joins' => [
            'text_translations' => ['texts.id', 'text_translations.text_id'],
        ],
    ];

    protected $fillable = [
        'key'
    ];

    public $translatedAttributes = [
        'value',
    ];
}
