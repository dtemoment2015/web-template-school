<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'title',
        'code_additional',
        'icon',
        'is_active'
    ];

    protected $casts = ['is_active' => 'boolean'];
}
