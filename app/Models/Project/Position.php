<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Nicolaslopezj\Searchable\SearchableTrait;

class Position extends Model implements TranslatableContract
{
    use HasFactory, Translatable, SearchableTrait;

    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'positions.code' => 4,
            'position_translations.title' => 10
        ],
        'joins' => [
            'position_translations' => ['positions.id', 'position_translations.position_id'],
        ],
    ];

    protected $fillable = [
        'code',
    ];

    public $translatedAttributes = [
        'title',
    ];
}
