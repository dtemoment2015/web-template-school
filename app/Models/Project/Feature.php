<?php

namespace App\Models\Project;

use App\Casts\ImageCast;
use App\Enums\FeatureType;
use App\Traits\UploadModelImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Feature extends Model implements TranslatableContract
{
    use HasFactory, Translatable, UploadModelImage;

    protected $fillable = [
        'is_active',
        'code',
        'type',
        'icon',
    ];

    protected $casts = [
        'is_active' => 'bool',
        // 'type' => FeatureType::class,
        'icon' => ImageCast::class
    ];

    public $translatedAttributes = [
        'title',
        'description',
    ];

    public function setIconAttribute($val)
    {
        $key = 'icon';
        $this->attributes[$key] = $this->loadImage($val, $key, 200);
    }
}
