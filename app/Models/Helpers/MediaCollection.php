<?php

namespace App\Models\Helpers;


use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MediaCollection extends Media
{

    protected $perPage = 50;

    protected $appends = ['path_small', 'path'];

    public function getPathAttribute()
    {
        return $this->getFullUrl();
    }

    public function getPathSmallAttribute()
    {
        if (
            optional(optional($this)->generated_conversions)['small']
        ) {
            return  $this->getFullUrl('small');
        }

        return "";
    }
}
