<?php

namespace App\Models;

use App\Casts\ImageCast;
use App\Models\Project\Position;
use App\Traits\UploadModelImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Nicolaslopezj\Searchable\SearchableTrait;

class Customer extends Authenticatable implements HasMedia, TranslatableContract
{

    const CODE_EXPIRED_SECOND = 180;

    use HasFactory, HasApiTokens, InteractsWithMedia, UploadModelImage, SearchableTrait, Translatable;

    public $translatedAttributes = [
        'body',
    ];

    protected $searchable = [
        'columns' => [
            'customers.PINFL' => 7,
            'customers.phone' => 10,
            'customers.first_name' => 6,
            'customers.last_name' => 6,
        ]
    ];

    protected $fillable = [
        'last_name',
        'first_name',
        'photo',
        'family_name',
        'password',
        'is_active',
        'is_root',
        'info',
        'code',
        'code_expired_at',
        'PINFL',
        'phone',

    ];

    protected $casts = [
        'is_active' => 'boolean',
        'is_root' => 'boolean',
        'photo' => ImageCast::class
    ];

    protected $hidden = [
        'password',
        'code',
    ];

    //RELATIONS:

    public function addresses()
    {
        return $this->morphMany(Address::class, 'model');
    }

    public function businesses()
    {
        return $this->belongsToMany(Business::class)
            ->withPivot('is_owner');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class)
            ->withPivot(['join_at', 'left_at']);
    }

    public function positions()
    {
        return $this->belongsToMany(Position::class)
            ->wherePivot('business_id',  request()->get('_business')->id); //show only - if exists business CARD.
    }

    //SET ATTRIBUTES

    public function setPasswordAttribute($val)
    {
        $this->attributes['password'] = bcrypt($val);
    }

    public function setCodeAttribute($val)
    {
        $this->attributes['code'] = bcrypt($val);
    }

    public function setPhotoAttribute($val)
    {
        $key = 'photo';
        $this->attributes[$key] = $this->loadImage($val, $key, 400);
    }

    //MEDIA
    public function registerMediaConversions(
        Media $media = null
    ): void {
        $this->addMediaConversion('medium')
            ->width(400)
            ->nonQueued();
    }
}
