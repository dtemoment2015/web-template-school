<?php

namespace App\Models\Business;

use App\Models\Business;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Question extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = [
        'code',
        'position',
    ];

    public $translatedAttributes = [
        'question',
        'answer',
    ];

    public function business()
    {
        return $this->belongsTo(Business::class);
    }
}
