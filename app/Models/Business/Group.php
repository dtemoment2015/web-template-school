<?php

namespace App\Models\Business;

use App\Models\Business;
use App\Models\Customer;
use App\Models\Project\Language;
use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    protected $fillable = [
        // 'locale',
        'code',
        'grouplist_id',
        'language_id',
    ];

    public function grouplist()
    {
        return $this->belongsTo(Grouplist::class);
    }

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function grouplists()
    {
        return $this->hasMany(Grouplist::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function students()
    {
        return $this->belongsToMany(Student::class)
            ->withPivot(['join_at', 'left_at']);
    }

    public function customers()
    {
        return $this->belongsToMany(Customer::class)
            ->withPivot(['join_at', 'left_at']);
    }
}
