<?php

namespace App\Models\Business;

use App\Traits\MultiPrimaryKeyTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentTranslation extends Model
{
    use HasFactory, MultiPrimaryKeyTrait;

    public $timestamps = false;

    public $incrementing = false;

    protected $primaryKey = [
        'album_id',
        'locale'
    ];

    protected $fillable = [
        'body',
    ];
}
