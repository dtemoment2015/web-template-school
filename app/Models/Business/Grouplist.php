<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grouplist extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'course',
        'title',
        'season_id',
    ];
    

    protected $appends = ['name'];

    public function getNameAttribute(){
        return optional($this)->course . ' ' . optional($this)->title;
    }

    public function season()
    {
        return $this->belongsTo(Season::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function albums()
    {
        return $this->belongsToMany(Album::class);
    }

    public function scopeCurrent($query)
    {
        return $query->whereHas('season', function ($q) {
            $q->whereIsCurrent(true);
        });
    }
}
