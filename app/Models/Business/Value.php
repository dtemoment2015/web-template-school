<?php

namespace App\Models\Business;

use App\Models\Project\Feature;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Value extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['value','feature_id','code'];

    public $translatedAttributes = [
        'title',
    ];

    public function feature()
    {
        return $this->belongsTo(Feature::class);
    }
}
