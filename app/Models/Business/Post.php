<?php

namespace App\Models\Business;

use App\Casts\ImageCast;
use App\Models\Business;
use App\Models\Helpers\MediaCollection;
use App\Models\Project\Rubric;
use App\Models\Project\Status;
use App\Traits\UploadModelImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Nicolaslopezj\Searchable\SearchableTrait;

class Post extends Model implements TranslatableContract, HasMedia
{
    use HasFactory, Translatable, InteractsWithMedia, UploadModelImage, SearchableTrait;

    // protected $dateFormat = 'U';

    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'posts.code' => 4,
            'post_translations.title' => 10
        ],
        'joins' => [
            'post_translations' => ['posts.id', 'post_translations.post_id'],
        ],
    ];

    protected $fillable = [
        'code',
        'position',
        'publication_at',
        'time_from',
        'time_till',
        'is_active',
        'season_id',
        'preview',
        'week_number',
        'group_id',
    ];

    protected $perPage = 30;

    // protected $with = ['mediaPreview'];

    protected $casts = [
        'is_active' => 'boolean',
        'publication_at' => 'datetime',
        'preview' => ImageCast::class
    ];

    public $translatedAttributes = [
        'title',
        'description',
        'body',
    ];

    public function contents()
    {
        return $this->hasMany(Content::class);
    }

    public function content()
    {
        return $this->morphOne(
            Content::class,
            'content'
        );
    }

    //weeks
    public function weeks()
    {
        return $this->belongsToMany(Status::class)->whereType('week');
    }

    public function season()
    {
        return $this->belongsTo(Season::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }
    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function grouplists()
    {
        return $this->belongsToMany(Grouplist::class);
    }

    public function albums()
    {
        return $this->belongsToMany(Album::class);
    }

    public function rubrics()
    {
        return $this->belongsToMany(Rubric::class);
    }

    //MEDIA
    public function mediaPreview()
    {
        return $this->morphOne(MediaCollection::class, 'model');
    }

    public function registerMediaConversions(
        Media $media = null
    ): void {

        $this->addMediaConversion('small')
            ->width(200)
            ->nonQueued();

        $this->addMediaConversion('medium')
            ->width(600)
            ->nonQueued();
    }

    public function setPreviewAttribute($val)
    {
        $key = 'preview';
        $this->attributes[$key] = $this->loadImage($val, $key, 600);
    }


    //temp 
    public function scopeHome(
        $query,
        $arg,
        $limit = 5
    ) {
        $query->limit($limit)
            ->whereHas('rubrics', function ($query) use ($arg) {
                $query->whereCode($arg);
            })
            ->orderBy('position')
            ->orderBy('publication_at')
            ->orderByDesc('id')
            ->whereIsActive(true);
    }
}
