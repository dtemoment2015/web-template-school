<?php

namespace App\Models\Business;

use App\Traits\MultiPrimaryKeyTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DescriptionTranslation extends Model
{
    use HasFactory, MultiPrimaryKeyTrait;

    public $timestamps = false;

    public $incrementing = false;

    protected $primaryKey = [
        'description_id',
        'locale'
    ];

    protected $fillable = [
        'title',
        'description'
    ];
}
