<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    use HasFactory;

    protected $table = 'feedback';

    protected $casts = [
        'is_active' => 'boolean'
    ];

    protected $fillable = [
        'ip',
        'user_agent',
        'form',
        'answer',
        'is_active',
    ];

    public function getAnswerAttribute($val)
    {
        return json_decode($val);
    }
}
