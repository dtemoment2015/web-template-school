<?php

namespace App\Models\Business;

use App\Models\Helpers\MediaCollection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Album extends Model implements TranslatableContract, HasMedia
{

    use HasFactory, Translatable, InteractsWithMedia;

    public $translatedAttributes = [
        'title',
        'description',
    ];

    protected $fillable = [
        'is_production',
        'position',
        'code',
    ];

    protected $casts  = [
        'is_production' => 'boolean'
    ];


    public function season()
    {
        return $this->belongsTo(Season::class);
    }

    public function grouplists()
    {
        return $this->belongsToMany(Grouplist::class);
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    //MEDIA
    public function mediaPreview()
    {
        return $this->morphOne(MediaCollection::class, 'model');
    }

    public function registerMediaConversions(
        Media $media = null
    ): void {

        $this->addMediaConversion('small')
            ->width(200)
            ->nonQueued();

        $this->addMediaConversion('medium')
            ->width(600)
            ->nonQueued();
    }
}
