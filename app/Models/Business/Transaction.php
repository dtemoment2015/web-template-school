<?php

namespace App\Models\Business;

use App\Models\Business;
use App\Models\Project\Payment;
use App\Models\Project\Status;
use App\Models\Project\Tariff;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'status',
        'is_active',
        'state',
        'uuid',
        'is_paid',
        'paid_at',
        'cancel_at',
        'from_at',
        'till_at',
        'currency',
        'price',
        'business_id',
        'tariff_id',
        'payment_id',
        'status_id',
        'comment',
        'llc',
        'address',
        'phone',
        'number',
        'bank_address',
        'mfo',
        'inn',
        'oked',
        'name',
    ];

    protected $appends = ['payment'];

    public function getPaymentAttribute()
    {
        return [
            'payme' => 'https://checkout.paycom.uz/' . base64_encode(
                'm=62a989dbc5c5a513f84a5c94;ac.oid=' .
                    $this->id . ';a=' . ($this->price * 100) .
                    ';l=ru;ct=500;cr=UZS'
            )
        ];
    }

    protected $casts = [
        'is_paid' => 'boolean',
        'is_active' => 'boolean',
        'till_at' => 'datetime',
        'from_at' => 'datetime',
        'paid_at' => 'datetime',
        'cancel_at' => 'datetime',
    ];

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function tariff()
    {
        return $this->belongsTo(Tariff::class);
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }
}
