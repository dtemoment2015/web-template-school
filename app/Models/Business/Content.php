<?php

namespace App\Models\Business;

use App\Enums\ContentType;
use App\Models\Form;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Database\Eloquent\Relations\Relation;

Relation::morphMap([
    'form' => Form::class,
    'post' => Post::class,
]);

class Content extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = [
        'body',
    ];

    protected $fillable = [
        'position',
        'type',
        'post_id',
        'value',
        'content_type',
        'content_id',
    ];

    protected $casts  = [
        'type' => ContentType::class
    ];

    public $timestamps = false;

    public function content()
    {
        return $this->morphTo();
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function form()
    {
        return $this->belongsTo(Form::class);
    }
}
