<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Season extends Model implements TranslatableContract
{

    use HasFactory, Translatable;

    protected $fillable = [
        'code',
        'from_at',
        'till_at',
        'is_current',
    ];

    protected $casts  = [
        'is_current' => 'boolean',
        'till_at' => 'datetime',
        'from_at' => 'datetime',

    ];

    public $translatedAttributes = [
        'title',
    ];
}
