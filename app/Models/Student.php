<?php

namespace App\Models;

use App\Models\Business\Group;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Student extends Authenticatable implements HasMedia
{
    use HasFactory, HasApiTokens, InteractsWithMedia, SearchableTrait;

    protected $searchable = [
        'columns' => [
            'students.PINFL' => 7,
            'students.phone' => 10,
            'students.first_name' => 6,
            'students.last_name' => 6,
        ]
    ];

    protected $fillable = [
        'last_name',
        'first_name',
        'family_name',
        'password',
        'is_active',
        'is_root',
        'info',
        'code',
        'PINFL',
        'phone',
    ];


    public $timestamps = false;

    protected $casts = [
        'is_active' => 'boolean',
        'is_root' => 'boolean',
    ];

    protected $hidden = [
        'password',
        'code',
    ];

    //RELATIONS
    public function addresses()
    {
        return $this->morphMany(Address::class, 'model');
    }

    public function businesses()
    {
        return $this->belongsToMany(Business::class);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class)
            ->withPivot(['join_at', 'left_at']);
    }

    //SET ATTRIBUTES

    public function setPasswordAttribute($val)
    {
        $this->attributes['password'] = bcrypt($val);
    }

    //MEDIA
    public function registerMediaConversions(
        Media $media = null
    ): void {
        $this->addMediaConversion('medium')
            ->width(400)
            ->nonQueued();
    }
}
