<?php

namespace App\Models;

use App\Models\Project\Location;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

Relation::morphMap([
    'business' => Business::class,
    'customer' => Customer::class,
    'student' => Student::class,
]);

class Address extends Model  implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = [
        'name',
        'address',
        'description',
    ];

    protected $casts = [
        'lat' => 'float',
        'lon' => 'float',
    ];

    protected $fillable = [
        'lat',
        'lon',
        'email',
        'location_id',
        'phone_1',
        'phone_2',
        'phone_3',
    ];

    protected $with = ['location.parent'];

    public function model()
    {
        return $this->morphTo();
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
