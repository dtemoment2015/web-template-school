<?php

namespace App\Models;

use App\Traits\MultiPrimaryKeyTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerTranslation extends Model
{
    use HasFactory, MultiPrimaryKeyTrait;

    public $timestamps = false;

    public $incrementing = false;

    protected $primaryKey = [
        'customer_id',
        'locale'
    ];

    protected $fillable = [
        'body',
    ];
}
