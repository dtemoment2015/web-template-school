<?php

namespace App\Models;

use App\Traits\MultiPrimaryKeyTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FieldTranslation extends Model
{
    use HasFactory, MultiPrimaryKeyTrait;

    public $timestamps = false;

    public $incrementing = false;

    protected $primaryKey = [
        'field_id',
        'locale'
    ];

    protected $fillable = [
        'label',
        'placeholder',
    ];
}
