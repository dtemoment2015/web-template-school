<?php

namespace App\Models;

use App\Casts\ImageCast;
use App\Models\Business\Album;
use App\Models\Business\Description;
use App\Models\Business\Domain;
use App\Models\Business\Feedback;
use App\Models\Business\Graduate;
use App\Models\Business\Group;
use App\Models\Business\Post;
use App\Models\Business\Question;
use App\Models\Business\Season;
use App\Models\Business\Style;
use App\Models\Business\Transaction;
use App\Models\Business\Value;
use App\Models\Project\Feature;
use App\Models\Project\Language;
use App\Traits\UploadModelImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Support\Str;
use App\Models\Helpers\MediaCollection;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * @property mixed $id
 * @property mixed $course_count
 */

class Business extends Model  implements TranslatableContract, HasMedia
{

    use HasFactory, SoftDeletes, UploadModelImage, Translatable, InteractsWithMedia;

    protected $casts = [
        'is_active' => 'boolean',
        'is_production' => 'boolean',
        'course_count' => 'int',
        'is_install' => 'int',
        'logo' => ImageCast::class
    ];

    public $translatedAttributes = [
        'title',
        'description',
    ];

    protected $fillable = [
        'name',
        'code',
        'logo',
        'course_count',
        'is_active',
        'is_production',
        'is_install',
    ];

    //RELATIONS 
    public function addresses()
    {
        return $this->morphMany(Address::class, 'model');
    }

    public function description()
    {
        return $this->hasOne(Description::class);
    }

    public function style()
    {
        return $this->hasOne(Style::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function customers()
    {
        return $this->belongsToMany(Customer::class);
    }

    public function students()
    {
        return $this->belongsToMany(Student::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class)
            ->limit(1)
            ->whereIsPaid(true)
            ->whereDate('till_at','>',now())
            ->orderByDesc('till_at');
    }

    public function seasons()
    {
        return $this->hasMany(Season::class);
    }

    public function albums()
    {
        return $this->hasMany(Album::class);
    }
    public function forms()
    {
        return $this->hasMany(Form::class);
    }

    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    public function graduates()
    {
        return $this->hasMany(Graduate::class);
    }

    public function values()
    {
        return $this->hasMany(Value::class)->whereHas('feature');
    }

    public function domains()
    {
        return $this->hasMany(Domain::class)
            ->orderByDesc('id');
    }

    public function questions()
    {
        return $this->hasMany(Question::class)
            ->orderBy('position')
            ->orderBy('id');
    }

    public function feedback()
    {
        return $this->hasMany(Feedback::class);
    }

    public function languages()
    {
        return $this->belongsToMany(Language::class)->orderBy('code');
    }

    public function features()
    {
        return $this->belongsToMany(Feature::class)
            ->whereIsActive(true)
            ->whereType('icon');
    }

    public function setLogoAttribute($val)
    {
        $key = 'logo';
        $this->attributes[$key] = $this->loadImage($val, $key, 400);
    }

    public function setNameAttribute($val)
    {

        $this->attributes['name'] = Str::lower($val);
    }

    public function setCodeAttribute($val)
    {
        $this->attributes['code'] = Str::lower($val);
    }

    //MEDIA
    public function mediaPreview()
    {
        return $this->morphOne(MediaCollection::class, 'model');
    }

    public function registerMediaConversions(
        Media $media = null
    ): void {

        $this->addMediaConversion('small')
            ->width(200)
            ->nonQueued();

        $this->addMediaConversion('medium')
            ->width(600)
            ->nonQueued();
    }
}
