<?php

namespace App\Providers;

use App\Models\Business;
use App\Models\Business\Grouplist;
use App\Models\Project\Text;
use App\Observers\BusinessObserver;
use App\Observers\GrouplistObserver;
use App\Observers\TextObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Grouplist::observe(GrouplistObserver::class);
        Text::observe(TextObserver::class);
        Business::observe(BusinessObserver::class);
    }
}
