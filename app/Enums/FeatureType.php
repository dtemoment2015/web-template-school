<?php

namespace App\Enums;

enum FeatureType:string
{
    case ICON = 'icon';
    case TEXT = 'text';
}
