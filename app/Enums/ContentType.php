<?php

namespace App\Enums;

enum ContentType:string
{
    case IMAGE = 'type-image';
    case GALLERY = 'type-gallery';
    case TEXT = 'type-text';
    case MOVIE = 'type-movie';
    case FORMDATA = 'type-formdata';
}
