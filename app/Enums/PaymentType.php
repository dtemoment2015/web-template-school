<?php

namespace App\Enums;

enum PaymentType: string
{
    case TRANSFER = 'transfer';
    case CASH = 'cash';
    case INSTALLMENT = 'installment';
    case TERMINAL = 'terminal';
}
