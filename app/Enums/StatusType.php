<?php

namespace App\Enums;

enum StatusType: string
{
    case TRANSACTION = 'transaction';
    case WEEK = 'week';
}
