<?php

namespace App\Traits;

use Exception;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

trait UploadModelImage
{
   public  function loadImage(
      $file = null,
      $key = '',
      $resize = 200,
   ) {

      if (!$file) return null;

      if (!$ID = $this->getKey()){
         $this->save();
         $ID = $this->getKey();
      }
      
      if(filter_var($file, FILTER_VALIDATE_URL)){
         return $this->getRawOriginal($key);
      }
      
      try {
         $img = Image::make($file)->resize(
            $resize,
            null,
            function ($constraint) {
               $constraint->aspectRatio();
            }
         );
         $mime = $img->mime();
         $extension = explode('/', $mime)[1];
         $fileName = $ID . '_' . time() . '_' . rand(1111, 9999)  . '.' . $extension;
         $folder = 'public/' . $this->table . '/' . $ID . '/';
         $oldFiles = Storage::allFiles($folder);
         Storage::delete($oldFiles);
         Storage::disk('local')->put($folder . $fileName, (string) $img->encode());

         return $fileName;
      } catch (Exception $e) {
         return $this->getRawOriginal($key);
      }
   }
}
