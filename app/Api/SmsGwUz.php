<?php

namespace App\Api;

use Illuminate\Support\Facades\Http;

class SmsGwUz
{
    public static function sendMessage(
        $phone = '',
        $message = ''
    ) {
        $phone =  str_replace('+', '',  $phone);
        $message =  trim($message);

        info('SEND SMS: ' . env('SMS_URL'));

        return  Http::acceptJson()
            ->post(env('SMS_URL'), [
                'login' =>  env('SMS_LOGIN'),
                'key' =>   env('SMS_KEY'),
                'sender' => env('SMS_SENDER'),
                'phone' => $phone,
                'text' => $message,
                "weight" => "10"
            ])->throw(function ($response, $e) {
                return null;
            })->json();
    }
}
