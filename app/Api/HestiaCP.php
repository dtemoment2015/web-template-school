<?php

namespace App\Api;

use Illuminate\Support\Facades\Http;
use Symfony\Component\Process\Process;

class HestiaCP
{
    const hst_hostname = 'app.edutoday.uz';
    const hst_port = '8083';
    const hst_hash = '';
    const hst_returncode = 'yes';

    public static function vAddDnsDomain($domain = '', $subdomain = true)
    {



        if (env('USE_HESTIA') == 'NO') {
            return false;
        }

        

        return  self::send(
            [
                // 'user' => self::hst_username,
                // 'password' => self::hst_password,
                'hash' => self::hst_hash,
                'returncode' => self::hst_returncode,
                'cmd' =>  'v-add-dns-domain',
                'arg1' => self::hst_username,
                'arg2' => $domain . ($subdomain == true ? '.edutoday.uz' : ''),
                'arg3' => '185.196.214.234',
                'arg4' => 'ns1.edutoday.uz',
                'arg5' => 'ns2.edutoday.uz',
                'arg6' => '',
                'arg7' => '',
                'arg8' => '',
                'arg9' => '',
                'arg10' => '',
                'arg11' => '',
                'arg12' => 'yes',
            ]
        );
    }

    

    public static function send($postvars = [])
    {
        // Send POST query via cURL
        $postdata = http_build_query($postvars);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://' . self::hst_hostname . ':' . self::hst_port . '/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
        $answer = curl_exec($curl);

        // Check result
        if (!$answer) {
            return null;
        } else {
            return "Query returned error code: " . $answer . "\n";
        }
    }
}
