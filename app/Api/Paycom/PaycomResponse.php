<?php

namespace App\Api\Paycom;

class PaycomResponse
{
    public function __construct(
        PaycomRequest $request
    ) {
        $this->request = $request;
    }

    public function send($result, $error = null)
    {
        $response['id'] = $this->request->id;
        $response['result'] = $result;
        if (optional($result)['id']) {
            $response['result']['transaction']  = $result['id'];
        }
        $response['error'] = $error;
        return response()->json($response, 200);
    }

    public function error($code, $message = null, $data = null)
    {
        throw new PaycomException(
            $this->request->id,
            $message,
            $code,
            $data
        );
    }
}
