<?php

namespace App\Jobs\v1;

use App\Models\Business\Domain;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class addSSLJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $domain;

    public function __construct($domain)
    {
        $this->domain = $domain;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        exec("/usr/local/hestia/bin/v-add-letsencrypt-domain denis " . $this->domain . " www." . $this->domain . "", $info, $result);
        
        if ($result == 0) {
            info('SET DOMAIN: ' . $this->domain);
            exec("/usr/local/hestia/bin/v-add-web-domain-ssl-force denis " . $this->domain . "");
            Domain::whereName($this->domain)->limit(1)->update(['is_active' => true]);
        } else {
            info('NO SET DOMAIN: ' . $this->domain);
            self::dispatch($this->domain)->onQueue('ssl');
        }
        sleep(10);
    }
}
