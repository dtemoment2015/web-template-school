<?php

namespace App\Jobs\v1;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class addBusinessJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $business;

    public function __construct($business)
    {
        $this->business = $business;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $domainName = $this->business->code . '.edutoday.uz';

        exec("/usr/local/hestia/bin/v-delete-dns-domain denis " . $domainName);

        exec("/usr/local/hestia/bin/v-delete-domain  denis " . $domainName);

        exec("/usr/local/hestia/bin/v-add-dns-domain denis " . $domainName . " 185.196.214.234 ns1.edutoday.uz ns2.edutoday.uz '' '' '' '' '' '' yes", $dnsOut, $dnsDone);
        if ($dnsDone == 0) {
            $this->business->done_dns = 1;
        }

        exec("/usr/local/hestia/bin/v-add-web-domain denis " . $domainName . " 185.196.214.234 www." . $domainName . "", $domainNameOut, $domainDone);
        if ($domainDone == 0) {
            $this->business->done_domain = 1;
        }

        exec("/usr/local/hestia/bin/v-add-web-domain-alias denis " . $domainName . " www." . $domainName . " yes", $aliasOut, $aliasDone);
        if ($aliasDone == 0) {
            $this->business->done_alias = 1;
        }

        exec("/usr/local/hestia/bin/v-add-web-domain-redirect denis " . $domainName . " " . $domainName . " 302 yes", $redirectOut, $redirectDone);
        if ($redirectDone == 0) {
            $this->business->done_redirect = 1;
        }

        $this->business->save();

        if ($this->business->done_dns && $this->business->done_domain && $this->business->done_alias && $this->business->done_redirect) {
            addSSLJob::dispatch($domainName)->onQueue('ssl');
        } else {
            self::dispatch($this->business)->onQueue('business_second');
        }

        sleep(10);
    }
}
