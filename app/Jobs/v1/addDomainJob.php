<?php

namespace App\Jobs\v1;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class addDomainJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $domain;

    public function __construct($domain)
    {
        $this->domain = $domain;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $domainName = $this->domain->name;

        exec("/usr/local/hestia/bin/v-delete-dns-domain denis " . $domainName);

        exec("/usr/local/hestia/bin/v-delete-domain  denis " . $domainName);

        exec("/usr/local/hestia/bin/v-add-dns-domain denis " . $domainName . " 185.196.214.234 ns1.edutoday.uz ns2.edutoday.uz '' '' '' '' '' '' yes", $dnsOut, $dnsDone);

        exec("/usr/local/hestia/bin/v-add-web-domain denis " . $domainName . " 185.196.214.234 www." . $domainName . "", $domainNameOut, $domainDone);

        exec("/usr/local/hestia/bin/v-add-web-domain-alias denis " . $domainName . " www." . $domainName . " yes", $aliasOut, $aliasDone);

        exec("/usr/local/hestia/bin/v-add-web-domain-redirect denis " . $domainName . " " . $domainName . " 302 yes", $redirectOut, $redirectDone);

        if ($dnsDone == 0 && $domainDone == 0 && $aliasDone == 0 && $redirectDone == 0) {
            // addSSLJob::dispatch($domainName)->onQueue('ssl');
        } else {
            self::dispatch($this->domain)->onQueue('domain');
        }
    }
}
