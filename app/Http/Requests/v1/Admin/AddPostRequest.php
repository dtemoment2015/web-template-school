<?php

namespace App\Http\Requests\v1\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'season_id' => 'sometimes|required|numeric',
            'group_id' => 'sometimes|required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'season_id.required' => __('Установите учебный сезон'),
            'season_id.numeric' => __('Установите учебный сезон'),
            'group_id.required' => __('Установите группу'),
            'group_id.numeric' => __('Установите группу'),
        ];
    }
}
