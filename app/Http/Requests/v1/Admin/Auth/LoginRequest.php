<?php

namespace App\Http\Requests\v1\Admin\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'phone' => 'required|regex:/(?:\+[9]{2}[8][0-9][0-9][0-9]{3}[0-9]{4})/u|string|max:13|exists:customers,phone',
            'code' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => __('message.phone.required'),
            'phone.regex' => __('message.phone.format'),
            'phone.max' => __('message.phone.format'),
            'phone.exists' => __('message.phone.repeat_request'),
            'code.required' => __('message.code.required'),
        ];
    }
}
