<?php

namespace App\Http\Requests\v1\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddBusiness extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'languages' => 'required|array|min:1',
            'code' => 'required|unique:businesses,code|string|max:20|min:1',
            'name' => 'required|unique:businesses,name|string|max:20|min:1',
            'season' => 'sometimes|required|string|max:100',
        ];
    }
}
