<?php

namespace App\Http\Requests\v1;

use Illuminate\Foundation\Http\FormRequest;

class GenerateTransferOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'llc' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'number' => 'required',
            'bank_address' => 'required',
            'mfo' => 'required',
            'inn' => 'required',
            'oked' => 'required',
        ];
    }
}
