<?php

namespace App\Http\Controllers\v1\Promo;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\LanguageResource;
use App\Http\Resources\v1\Admin\PostResource;
use App\Http\Resources\v1\Admin\TextResource;
use App\Models\Business\Post;
use App\Models\Project\Language;
use App\Models\Project\Tariff;
use App\Models\Project\Text;
use Illuminate\Support\Facades\Cache;

class IndexController extends Controller
{
    public function index()
    {
        $lang = app()->getLocale();

        $post = Post::whereDoesntHave('business')->orderBy('position');

        return response()->json(
            [
                'text' =>  Cache::rememberForever('text_' . $lang, function () {
                    return TextResource::collection(Text::all())->collection->pluck('value', 'key')->jsonSerialize();
                }),
                'localizations' => Cache::rememberForever('localizations_' . $lang, function () {
                    return  LanguageResource::collection(Language::orderBy('code')->whereIsActive(true)->get())->jsonSerialize();
                }),
                'pages' => $post->clone()->home('promo_page', 30)->get(),
                'benefits' => $post->clone()->home('promo_benefit', 30)->get(),
                'features' => $post->clone()->home('promo_feature', 30)->get(),
                'tariffs' => Tariff::whereIsActive(true)->orderBy('price')->where('price', '>', 0)->get(),
            ]
        );
    }

    public function show($code, Post $post)
    {
        return new PostResource(
            $post->whereCode($code)
                ->whereIsActive(true)
                ->limit(1)
                ->firstOrFail()
        );
    }
}
