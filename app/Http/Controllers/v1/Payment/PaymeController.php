<?php

namespace App\Http\Controllers\v1\Payment;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Payment\Paycom\PaycomCancelResource;
use App\Http\Resources\v1\Payment\Paycom\PaycomCreateResource;
use App\Http\Resources\v1\Payment\Paycom\PaycomPerformResource;
use App\Http\Resources\v1\Payment\Paycom\PaycomResource;
use App\Http\Resources\v1\Payment\Paycom\PaycomTransactionResource;
use App\Jobs\v1\PaidJob;
use App\Api\Paycom\PaycomResponse;
use App\Api\Paycom\PaycomRequest;
use App\Api\Paycom\PaycomTransaction;
use App\Api\Paycom\PaycomException;
use App\Models\Business\Transaction;

class PaymeController extends Controller
{
    public $request;
    public $response;

    public function __construct()
    {
        $this->request = new PaycomRequest();
        $this->response = new PaycomResponse($this->request);
        $this->Transaction = new Transaction();
    }

    /**
     * Authorizes session and handles requests.
     */
    public function index()
    {
        try {
            switch (optional($this->request)->method) {
                case 'CheckPerformTransaction':
                    return $this->CheckPerformTransaction();
                    break;
                case 'CheckTransaction':
                    return $this->CheckTransaction();
                    break;
                case 'CreateTransaction':
                    return $this->CreateTransaction();
                    break;
                case 'PerformTransaction':
                    return $this->PerformTransaction();
                    break;
                case 'CancelTransaction':
                    return $this->CancelTransaction();
                    break;
                case 'GetStatement':
                    return $this->GetStatement();
                    break;
                default:
                    $this->response->error(
                        PaycomException::ERROR_METHOD_NOT_FOUND,
                        'Method not found.',
                        optional($this->request)->method
                    );
                    break;
            }
        } catch (PaycomException $exc) {
            return $exc->send();
        }
    }

    private function CheckPerformTransaction()
    {
        $transaction = Transaction::whereId(optional($this->request->params)['account']['oid'])->limit(1)->first();

        $this->validateData($transaction);

        return $this->response->send(
            [
                'allow' => true
            ]
        );
    }

    private function CheckTransaction()
    {
        $transaction = $this->Transaction
            ->whereUuid($this->request->params['id'])
            ->first();

        if (!$transaction) {
            $this->response->error(
                PaycomException::ERROR_TRANSACTION_NOT_FOUND,
                'Transaction not found.'
            );
        }

        return $this->response->send(new PaycomResource($transaction));
    }

    private function CreateTransaction()
    {
        /** Информация о заказе */
        $transaction = Transaction::whereId($this->request->params['account']['oid'])
            ->limit(1)
            ->first();

        $this->validateData($transaction);

        if (
            ($this->request->params['id'] == $transaction->uuid)
            && (
                (optional($transaction->created_at)->timestamp * 1000 + PaycomTransaction::TIMEOUT)  <  time() * 1000
                || $transaction->state != 1
            )
        ) {
            /** Не прошло ли 24 часа после создания */
            $this->response->error(
                PaycomException::ERROR_COULD_NOT_PERFORM,
                PaycomException::message(
                    'Статус транзакции не соотвествующий!',
                    'Статус транзакции не соотвествующий!',
                    'Статус транзакции не соотвествующий!'
                ),
                'Timout'
            );
        } elseif (
            $transaction->uuid != $this->request->params['id']
            && $transaction->state == 1
            && (optional($transaction->created_at)->timestamp * 1000 + 60 * 1000)  >  time() * 1000
        ) {
            $this->response->error(
                PaycomException::ERROR_INVALID_ACCOUNT,
                PaycomException::message(
                    'Оплата в очереди, попробуйте через 1 минуту повторить запрос',
                    'Оплата в очереди, попробуйте через 1 минуту повторить запрос',
                    'Оплата в очереди, попробуйте через 1 минуту повторить запрос'
                ),
                'Timout'
            );
        } elseif ($transaction->is_paid) {
            $this->response->error(
                PaycomException::ERROR_INVALID_ACCOUNT,
                PaycomException::message(
                    'Чек уже оплачен',
                    'Чек уже оплачен',
                    'Чек уже оплачен'
                ),
                'Timout'
            );
        }

        if (!$transaction->uuid || $transaction->uuid != $this->request->params['id']) {
            $transaction->created_at = now();
            $transaction->cancel_at = null;
            $transaction->paid_at = null;
            $transaction->comment = null;
            $transaction->is_paid = false;
        }

        $transaction->uuid = $this->request->params['id'];
        $transaction->state = 1;
        $transaction->save();

        /** Если транзакция уже оплачена */
        if ($transaction->is_paid) {
            $this->response->error(
                PaycomException::ERROR_COULD_NOT_PERFORM,
                PaycomException::message(
                    'Статус транзакции не соотвествующий',
                    'Статус транзакции не соотвествующий',
                    'Статус транзакции не соотвествующий'
                ),
                'State'
            );
        } else {
            return $this->response->send(
                new PaycomCreateResource($transaction)
            );
        }

        $this->response->error(
            PaycomException::ERROR_INVALID_ACCOUNT,
            PaycomException::message(
                'Блокируется паралельная транзация',
                'Блокируется паралельная транзация',
                'Блокируется паралельная транзация'
            ),
            'Sync'
        );
    }

    private function PerformTransaction()
    {

        $transaction = Transaction::whereUuid($this->request->params['id'])
            ->limit(1)
            ->first();

        if (!$transaction) $this->response->error(
            PaycomException::ERROR_TRANSACTION_NOT_FOUND,
            'Transaction not found.'
        );
        switch ($transaction->state) {
            case PaycomTransaction::STATE_CREATED:
                if ((optional($transaction->created_at)->timestamp * 1000 + PaycomTransaction::TIMEOUT)  <  time() * 1000) {
                    $transaction->state = PaycomTransaction::REASON_CANCELLED_BY_TIMEOUT;
                    $transaction->save();
                    $this->response->error(
                        PaycomException::ERROR_COULD_NOT_PERFORM,
                        'Transaction is expired.'
                    );
                } else {
                    $transaction->state = PaycomTransaction::STATE_COMPLETED;
                    $transaction->paid_at = now();
                    $transaction->from_at = now();
                    $transaction->till_at = now()->clone()->addDays((int) $transaction->tariff->period)->endOfDay();
                    $transaction->business()->update(['is_active' => true]);
                    $transaction->is_paid = true;
                    $transaction->save();
                    PaidJob::dispatch($transaction)->onQueue('payment');
                    //PAY JOB ADD
                    return $this->response->send(new PaycomPerformResource($transaction));
                }
                break;
            case PaycomTransaction::STATE_COMPLETED:
                return $this->response->send(new PaycomPerformResource($transaction));
                break;
            default:
                $this->response->error(
                    PaycomException::ERROR_COULD_NOT_PERFORM,
                    'Could not perform this operation.'
                );
                break;
        }
    }

    private function CancelTransaction()
    {
        $transaction = Transaction::whereUuid($this->request->params['id'])
            ->limit(1)
            ->first();

        if (!$transaction) $this->response->error(
            PaycomException::ERROR_TRANSACTION_NOT_FOUND,
            'Transaction not found.'
        );
        switch ($transaction->state) {
            case PaycomTransaction::STATE_CANCELLED:
                return $this->response->send(new PaycomCancelResource($transaction));
                break;
            case PaycomTransaction::STATE_CANCELLED_AFTER_COMPLETE:
                return $this->response->send(new PaycomCancelResource($transaction));
                break;
            case PaycomTransaction::STATE_CREATED:
                $transaction->cancel_at = now();
                $transaction->state = -1;
                $transaction->comment = 3;
                $transaction->save();
                return $this->response->send(new PaycomCancelResource($transaction));
                break;
            case PaycomTransaction::STATE_COMPLETED:
                $transaction->cancel_at = now();
                $transaction->state = -2;
                $transaction->comment = 5;
                $transaction->is_paid = false;
                $transaction->save();
                return $this->response->send(new PaycomTransactionResource($transaction));
                break;
        }
    }

    private function GetStatement()
    {
        if (!isset($this->request->params['from'])) {
            $this->response->error(PaycomException::ERROR_INVALID_ACCOUNT, 'Incorrect period.', 'from');
        }

        if (!isset($this->request->params['to'])) {
            $this->response->error(PaycomException::ERROR_INVALID_ACCOUNT, 'Incorrect period.', 'to');
        }

        if (1 * $this->request->params['from'] >= 1 * $this->request->params['to']) {
            $this->response->error(PaycomException::ERROR_INVALID_ACCOUNT, 'Incorrect period. (from >= to)', 'from');
        }

        return $this->response->send(['transactions' => PaycomTransactionResource::collection(Transaction::limit(10)
            ->whereCreatedAt($this->request->params['from'])
            ->wherePaidAt($this->request->params['to'])
            ->get())]);
    }

    private function validateData($Transaction)
    {
        if (!is_numeric(optional($this->request->params)['amount'])) {
            throw new PaycomException(
                $this->request->id,
                'Incorrect amount.',
                PaycomException::ERROR_INVALID_AMOUNT
            );
        }
        if ($Transaction) {
            if ((int) $Transaction->price * 100 != (int) optional($this->request->params)['amount']) {
                throw new PaycomException(
                    $this->request->id,
                    'Incorrect amount.',
                    PaycomException::ERROR_INVALID_AMOUNT
                );
            }
        } else {
            throw new PaycomException(
                $this->request->id,
                PaycomException::message(
                    'Неверный код заказа.',
                    'Harid kodida xatolik.',
                    'Incorrect order code.'
                ),
                PaycomException::ERROR_INVALID_ACCOUNT,
                'order_id'
            );
        }
    }
}
