<?php

namespace App\Http\Controllers\v1\Client;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\PostResource;
use Illuminate\Http\Request;

class ClientPostController extends Controller
{

    public function index(Request $request)
    {
        return PostResource::collection(
            $request->get('_business')
                ->posts()
                ->whereIsActive(true)
                ->when($code  = optional($request)->code, function ($query) use ($code) {
                    $query->whereHas('rubrics', function ($queryRubric) use ($code) {
                        $queryRubric->whereCode($code);
                    });
                })
                ->when(request('code')=='graduate',function($query){
                    $query->orderByTranslation('title');
                })
                ->when(optional($request)->q, function ($query) use ($request) {
                    $query->search($request->q)
                        ->whereHas('rubrics', function ($queryRubric) {
                            $queryRubric->whereIn('code', ['new', 'event']);
                        })
                        ->orderBy('relevance', 'DESC');
                })
                ->orderByDesc('posts.publication_at')
                ->groupBy('posts.id')
                ->paginate()
        );
    }

    public function show(Request $request, $id)
    {
        return new PostResource(
            $request->get('_business')
                ->posts()
                ->with(['media', 'season', 'grouplists', 'group.grouplist', 'contents.content', 'rubrics'])
                ->whereIsActive(true)
                ->whereCode($id)
                ->limit(1)
                ->firstOrFail()
        );
    }
}
