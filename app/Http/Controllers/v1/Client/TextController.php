<?php

namespace App\Http\Controllers\v1\Client;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\TextResource;
use App\Models\Project\Text;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class TextController extends Controller
{

    public function index()
    {
        $lang = app()->getLocale();
        return Cache::rememberForever('text_' . $lang, function () {
            return TextResource::collection(Text::all())->collection->pluck('value', 'key')->jsonSerialize();
        });
    }

    public function store(Request $request, Text $text)
    {
        // Cache::flush();
        return response()->json(
            [
                'value' => $text->firstOrCreate([
                    'key' => $request->key
                ], [
                    'key' => $request->key,
                    'value:ru' => $request->value
                ])->value
            ]
        );
    }
}
