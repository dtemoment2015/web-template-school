<?php

namespace App\Http\Controllers\v1\Client;

use App\Http\Controllers\Controller;
use App\Http\Resources\BusinessResource;
use App\Http\Resources\v1\Admin\LanguageResource;
use App\Models\Business;
use App\Models\Project\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class IndexController extends Controller
{

    public function index(Request $request)
    {
        $business = $request->get('_business');
        $lang = $request->get('_lang');
        return   Cache::rememberForever('business_' . $business->id . $lang, function () use ($business) {
            return (new BusinessResource($business->load('media')))->jsonSerialize();
        });
    }

    public function languages()
    {
        $business =  request()->get('_business');
        return Cache::rememberForever('business_languages' . $business->id, function () use ($business) {
            return  LanguageResource::collection(
                $business->languages()->whereIsActive(true)->get()
            )->collection->pluck('title', 'code')->jsonSerialize();
        });
    }
}
