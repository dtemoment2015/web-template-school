<?php

namespace App\Http\Controllers\v1\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClientFeedbackController extends Controller
{
    public function store(Request $request)
    {
        $answers = [];

        foreach ($request->answer as $key =>  $answer) {
            $answers[] =
                [
                    'title' => $key,
                    'answer' => $answer
                ];
        }

        $request->get('_business')->feedback()->create(
            [
                'ip' => $request->ip(),
                'form' => optional($request)->title,
                'answer' => json_encode($answers)
            ]
        );
    }
}
