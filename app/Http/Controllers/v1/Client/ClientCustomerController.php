<?php

namespace App\Http\Controllers\v1\Client;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\CustomerResource;
use Illuminate\Http\Request;

class ClientCustomerController extends Controller
{
    public function index(Request $request)
    {
        return CustomerResource::collection(
            $request->get('_business')
                ->customers()
                ->with('positions')
                ->whereHas('positions')
                ->whereNotNull('first_name')
                ->whereNotNull('last_name')
                ->paginate()
        );
    }

    public function show(Request $request, $id)
    {
        return new CustomerResource(
            $request->get('_business')
                ->customers()
                ->with(['positions'])
                // ->whereIsActive(true)
                ->whereId($id)
                ->limit(1)
                ->firstOrFail()
        );
    }
}
