<?php

namespace App\Http\Controllers\v1\Order;

use App\Http\Controllers\Controller;
use App\Models\Business\Transaction;
use Barryvdh\DomPDF\Facade as PDF;
use NumberFormatter;

class IndexController extends Controller
{
    public function exportPdf()
    {
        $transaction = Transaction::with('tariff')->findOrFail(request('id'));

        return view('order', ['data' => $transaction]);
        $pdf = PDF::loadView('order', ['data' => $transaction]);
        $path = storage_path('app/public/orders');
        $fileName =  time() . '.' . 'pdf';
        $pdf->save($path . '/' . $fileName);
        return response()->json(['path' => url('storage/orders/' . $fileName)]);
    }
}
