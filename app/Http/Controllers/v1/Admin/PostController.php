<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\PostResource;
use App\Models\Business\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(Post $post)
    {
        return PostResource::collection($post->with('rubrics')->whereDoesntHave('business')->paginate());
    }

    public function store(Request $request, Post $post)
    {


        $item = $post->create($request->all());

        if ($rubrics = (array) optional($request)->rubrics) {
            $item->rubrics()->sync(
                $rubrics
            );
        }



        return new PostResource($item);
    }

    public function show(Post $post)
    {
        return new PostResource($post->load('rubrics'));
    }

    public function update(Request $request, Post $post)
    {
        $post->update($request->all());
        if ($rubrics = (array) optional($request)->rubrics) {
            $post->rubrics()->sync(
                $rubrics
            );
        }
        return new PostResource($post);
    }

    public function destroy(Post $post)
    {
        return response()->json(
            ['deleted' => (bool)$post->delete()]
        );
    }
}
