<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\FormResource;
use App\Models\Form;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index(Form $Form)
    {
        return FormResource::collection($Form->paginate());
    }

    public function store(Request $request, Form $Form)
    {
        return new FormResource($Form->create($request->all()));
    }

    public function show(Form $Form)
    {
        return new FormResource($Form);
    }

    public function update(Request $request, Form $Form)
    {
        $Form->update($request->all());

        return new FormResource($Form);
    }

    public function destroy(Form $Form)
    {
        return response()->json(
            ['deleted' => (bool)$Form->delete()]
        );
    }
}
