<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\LanguageResource;
use App\Models\Project\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function index(Language $language)
    {
        return LanguageResource::collection($language->paginate());
    }

    public function store(Request $request, Language $language)
    {
        return new LanguageResource($language->create($request->all()));
    }

    public function show(Language $language)
    {
        return new LanguageResource($language);
    }

    public function update(Request $request, Language $language)
    {
        $language->update($request->all());

        return new LanguageResource($language);
    }

    public function destroy(Language $language)
    {
        return response()->json(
            ['deleted' => (bool)$language->delete()]
        );
    }
}
