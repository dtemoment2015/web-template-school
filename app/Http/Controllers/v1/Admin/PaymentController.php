<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\PaymentResource;
use App\Models\Project\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function index(Payment $payment)
    {
        return PaymentResource::collection($payment->paginate());
    }

    public function store(Request $request, Payment $payment)
    {
        return new PaymentResource($payment->create($request->all()));
    }

    public function show(Payment $payment)
    {
        return new PaymentResource($payment);
    }

    public function update(Request $request, Payment $payment)
    {
        $payment->update($request->all());

        return new PaymentResource($payment);
    }

    public function destroy(Payment $payment)
    {
        return response()->json(
            ['deleted' => (bool)$payment->delete()]
        );
    }
}
