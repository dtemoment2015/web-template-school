<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\FeatureResource;
use App\Models\Project\Feature;
use Illuminate\Http\Request;

class FeatureController extends Controller
{

    public function index(Feature $feature)
    {
        return FeatureResource::collection($feature->paginate());
    }

    public function store(Request $request, Feature $feature)
    {
        return new FeatureResource($feature->create($request->all()));
    }

    public function show(Feature $feature)
    {
        return new FeatureResource($feature);
    }

    public function update(Request $request, Feature $feature)
    {
        $feature->update($request->all());
        
        return new FeatureResource($feature);
    }

    public function destroy(Feature $feature)
    {
        return response()->json(
            ['deleted' => (bool)$feature->delete()]
        );
    }
}
