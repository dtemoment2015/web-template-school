<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\FieldResource;
use App\Models\Field;
use Illuminate\Http\Request;

class FieldController extends Controller
{
    public function index(Field $field)
    {
        return FieldResource::collection($field->paginate());
    }

    public function store(Request $request, Field $field)
    {
        return new FieldResource($field->create($request->all()));
    }

    public function show(Field $field)
    {
        return new FieldResource($field);
    }

    public function update(Request $request, Field $field)
    {
        $field->update($request->all());
        return new FieldResource($field);
    }

    public function destroy(Field $field)
    {
        return response()->json(
            ['deleted' => (bool)$field->delete()]
        );
    }
}
