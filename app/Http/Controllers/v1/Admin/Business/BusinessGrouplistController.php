<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\GrouplistResource;
use App\Models\Business\Grouplist;
use Illuminate\Http\Request;

class BusinessGrouplistController extends Controller
{
    public function index()
    {
        return GrouplistResource::collection(Grouplist::whereHas(
            'group.business',
            function ($query)  {
                $query->whereId(
                    request()->get('_business')->id
                );
            }
        )->with(['season'])
        ->orderByDesc('season_id')
        ->orderBy('course')
        ->orderBy('title')
        ->paginate());
    }
}
