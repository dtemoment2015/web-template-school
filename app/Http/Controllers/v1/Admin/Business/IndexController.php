<?php

namespace App\Http\Controllers\v1\Admin\Business;

// use App\Api\HestiaCP;
use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\AddBusiness;
use App\Http\Requests\v1\Admin\UpdateBusiness;
use App\Http\Resources\v1\Admin\BusinessAccountResource;
use App\Http\Resources\v1\Admin\BusinessResource;
use App\Jobs\v1\addBusinessJob;
use App\Jobs\v1\addDomainJob;
use App\Jobs\v1\addSSLJob;
use App\Models\Business\Transaction;
use App\Models\Project\Tariff;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public $business = null;

    public function __construct()
    {
        $this->business = optional(auth('customer')->user())->businesses();
    }

    public function index()
    {
        return BusinessResource::collection($this->business->orderByDesc('id')->paginate(100));
    }

    public function show($id)
    {
        return new BusinessAccountResource($this->business->whereId($id)->with(['languages', 'media'])->firstOrFail());
    }

    public function store(AddBusiness $request)
    {
        if ($business =  DB::transaction(function () use ($request) {
            $business =   $this->business->create(
                $request->all()
            );
            $business->languages()->sync((array)  optional($request)->languages);
            $seasons = [];
            foreach ($business->languages as $lang) {
                $seasons['title:' . $lang->code] = $request->season;
            }
            $business->seasons()->create($seasons);
            if ($this->business->count() == 1) {

                if ($tariff = Tariff::whereCode('trial')->limit(1)->whereIsActive(true)->first()) {
                    $now = now();

                    $business->transactions()->create(
                        [
                            'price' => $tariff->price,
                            'is_paid' => 1,
                            'is_active' => 1,
                            'tariff_id' => $tariff->id,
                            'currency' => $tariff->currency,
                            'paid_at' => $now,
                            'from_at' => $now,
                            'till_at' => $now->clone()->addDays((int) $tariff->period)->endOfDay(),
                            'state' => 2,
                        ]
                    );
                }

                $business->is_active = 1;

                $business->save();
            }
            return $business;
        })) {
            addBusinessJob::dispatch($business)->onQueue('business');
            return new BusinessAccountResource($business);
        }

        return response()->json(['message' => __('message.create.error')], 422);
    }

    public function update(UpdateBusiness $request, $id)
    {
        $business = $this->business->whereId($id)->firstOrFail();

        $business->update($request->all());

        $business->features()->sync((array)  (array)optional($request)->icon);

        if ($text = optional($request)->text) {
            foreach ($text as $t) {
                $business->values()->updateOrCreate(
                    ['feature_id' => $t['feature_id']],
                    [
                        'feature_id' => $t['feature_id'],
                        'value' => $t['value'],
                        'code' => $t['code'],
                    ],
                );
            }
        }

        $business->languages()->sync((array) optional($request)->languages);

        foreach (config('translatable.locales') as $lang) {
            Cache::forget('business_' . $business->id . $lang);
            Cache::forget('business_languages' . $business->id);
            Cache::forget('business_domain_' . $business->name . '.edutoday.uz' . $lang);
        }
        foreach ($business->domains as $domain) {
            foreach (config('translatable.locales') as $lang) {
                Cache::forget('business_domain_' . $domain->name . $lang);
            }
        }

        return new BusinessAccountResource($business->load('media'));
    }
}
