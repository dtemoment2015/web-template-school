<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\AddressResource;
use Illuminate\Http\Request;

class BusinessAddressController extends Controller
{
    public function __construct()
    {
        parent::__construct('addresses');
    }

    public function index()
    {
        return AddressResource::collection($this->item()->paginate());
    }

    public function store(Request $request)
    {
        return new AddressResource($this->item()->create($request->all()));
    }

    public function show($id)
    {
        return new AddressResource($this->item()->whereId($id)->firstOrFail());
    }

    public function update(Request $request, $id)
    {
        $customer =  $this->item()->whereId($id)->firstOrFail();
        $customer->update($request->all());
        return new AddressResource($customer);
    }

    public function destroy($id)
    {
        return response()->json(
            ['deleted' => (bool)$this->item()->whereId($id)->delete()]
        );
    }
}
