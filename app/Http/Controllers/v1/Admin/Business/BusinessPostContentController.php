<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\ContentResource;
use App\Models\Business\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BusinessPostContentController extends Controller
{
    public function index($postID)
    {
        $post = request()
            ->get('_business')
            ->posts()
            ->orderBy('position')
            ->findOrFail($postID);

        return ContentResource::collection(
            $post->contents()->with('content')->get()
        );
    }

    public function store(Request $request, $postID)
    {
        $post = DB::transaction(function () use ($request, $postID) {
            $post = request()
                ->get('_business')
                ->posts()
                ->findOrFail($postID);
            $post->contents()->delete();
            $post->contents()->createMany((array) optional($request)->data);
            return $post;
        });

        return ContentResource::collection(
            $post->contents()->with('content')->orderBy('position')->get()
        );

    }
}
