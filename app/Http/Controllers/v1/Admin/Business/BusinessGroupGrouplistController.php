<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\GrouplistResource;
use Illuminate\Http\Request;

class BusinessGroupGrouplistController extends Controller
{

    public function __construct()
    {
        parent::__construct('groups');
    }

    public function index($groupId)
    {
        return GrouplistResource::collection($this->item()->whereId($groupId)->firstOrFail()->grouplists()->with(['season'])->orderByDesc('id')->paginate());
    }

    public function store(Request $request, $groupId)
    {
        return new GrouplistResource($this->item()->whereId($groupId)->firstOrFail()->grouplists()->create($request->all()));
    }

    public function show($groupId, $id)
    {
        return new GrouplistResource($this->item()->whereId($groupId)->firstOrFail()->grouplists()->whereId($id)->firstOrFail());
    }

    public function update(Request $request, $groupId, $id)
    {
        $customer =  $this->item()->whereId($groupId)->firstOrFail()->grouplists()->whereId($id)->firstOrFail();
        $customer->update($request->all());
        return new GrouplistResource($customer);
    }

    public function destroy($groupId, $id)
    {
        return response()->json(
            ['deleted' => (bool)$this->item()->whereId($groupId)->firstOrFail()->grouplists()->whereId($id)->delete()]
        );
    }
}
