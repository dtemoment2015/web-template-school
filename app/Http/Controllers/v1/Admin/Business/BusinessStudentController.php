<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\StudentResource;
use App\Models\Student;
use Illuminate\Http\Request;

class BusinessStudentController extends Controller
{

    public function __construct()
    {
        parent::__construct('students');
    }

    public function index()
    {
        return StudentResource::collection($this->item()
            ->when($search = request('search'), function ($query) use ($search) {
                $query->search($search)
                    ->orderBy('relevance', 'DESC');
            })
            ->orderBy('last_name')
            ->orderBy('first_name')
            ->groupBy('students.id')
            ->paginate());
    }

    public function store(Request $request)
    {

        $item = Student::firstOrCreate(
            ['phone' => $request->phone],
            $request->all()
        );

        $request->_business->students()->syncWithoutDetaching(
            [$item->id]
        );

        return new StudentResource(
            $item
        );
    }

    public function show($id)
    {
        return new StudentResource($this->item()->whereId($id)->firstOrFail());
    }

    public function update(Request $request, $id)
    {
        $customer =  $this->item()->whereId($id)->firstOrFail();
        $customer->update($request->all());
        return new StudentResource($customer);
    }

    public function destroy($id)
    {
        // return response()->json(
        //     ['deleted' => (bool)$this->item()->whereId($id)->delete()]
        // );
    }
}
