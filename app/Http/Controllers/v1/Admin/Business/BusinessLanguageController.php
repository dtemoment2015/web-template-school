<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\LanguageResource;
use Illuminate\Http\Request;

class BusinessLanguageController extends Controller
{
    public function index(Request $request)
    {
        return LanguageResource::collection($request->_business->languages()->paginate);
    }
}
