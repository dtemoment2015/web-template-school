<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Models\Business\Feedback;
use App\Models\Business\Post;
use App\Models\Business\Season;
use Illuminate\Http\Request;

class BusinessStatController extends Controller
{
    public function index(Request $request)
    {
        $business = $request->get('_business')
            ->loadCount([
                'posts', 'graduates', 'feedback', 'customers'
            ]);

        return response()->json(
            [
                [
                    'key' => 'posts',
                    'title' => 'Всего постов',
                    'icon' => 'ti-shopping-cart-full',
                    'value' => $business->posts_count
                ],
                [
                    'key' => 'graduates',
                    'title' => 'Выпускные группы',
                    'icon' => 'ti-id-badge',
                    'value' => $business->graduates_count
                ],
                [
                    'key' => 'feedback',
                    'title' => 'Всего обращений',
                    'icon' => 'ti-id-badge',
                    'value' => $business->feedback_count
                ],
                [
                    'key' => 'customers',
                    'title' => 'Всего пользователей',
                    'icon' => 'ti-id-badge',
                    'value' => $business->customers_count
                ],
            ]
        );
    }
}
