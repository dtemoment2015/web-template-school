<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\CustomerResource;
use Illuminate\Http\Request;

class BusinessGroupCustomerController extends Controller
{
    public function __construct()
    {
        parent::__construct('groups');
    }

    public function index($groupId)
    {
        return CustomerResource::collection(
            $this->item()
                ->whereId($groupId)
                ->firstOrFail()
                ->customers()
                ->orderBy('last_name')
                ->orderBy('first_name')
                ->wherePivot('left_at', null)
                ->paginate()
        );
    }

    public function store(Request $request, $groupId)
    {


        $group  =   $this
        ->item()
        ->whereId($groupId)
        ->firstOrFail();

        if ($group->customers()->wherePivot('left_at', null)->exists()) {
            return response()->json(['message' => __('Учитель уже числится в этой группе')], 422);
        }

        if (optional($request)->customer_id) {
         $group
                ->customers()
                ->attach(
                    [$request->customer_id => [
                        'join_at' => now(),
                    ]]
                );
        }

        return response()->json(['data' => [
            'id' => true
        ]]);
    }

    public function destroy($groupId, $id)
    {
        return response()->json(
            [
                'deleted' =>  $this
                    ->item()
                    ->whereId($groupId)
                    ->firstOrFail()
                    ->customers()
                    ->wherePivot('left_at', null)
                    ->syncWithoutDetaching(
                        [$id => [
                            'left_at' => now()
                        ]]
                    )
            ]
        );
    }
}
