<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\MediaResource;
use Illuminate\Http\Request;
use App\Models\Helpers\MediaCollection;

class BusinessMediaController extends Controller
{

    public function index()
    {
        $item = $this->getStoryPost();
        return MediaResource::collection($item->media()->orderByDesc('order_column')->paginate())->additional(
            [
                'post' => $item,
            ]
        );
    }

    public function store(Request $request)
    {
        $item = $this->getStoryPost();
        $mediaList = [];
        if (($media = optional($request)->media) &&
            is_array($media) &&
            count($media)
        ) {
            foreach ($media as $m) {
             $mediaList[] =   $item->addMedia($m)->toMediaCollection('posts');
            }
        }

        return MediaResource::collection($mediaList);
    }

    public function destroy($id, MediaCollection $media)
    {
        return response()->json(
            ['deleted' => (bool) $media->whereId($id)->delete()]
        );
    }

    private function getStoryPost()
    {
        return request()
            ->get('_business')
            ->posts()
            ->firstOrCreate([
                'code' => 'story___'
            ]);
    }
}
