<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\GroupResource;
use Illuminate\Http\Request;

class BusinessGroupController extends Controller
{

    public function __construct()
    {
        parent::__construct('groups');
    }

    public function index()
    {
        return GroupResource::collection($this->item()->with(['grouplist', 'language'])->orderByDesc('id')->paginate());
    }

    public function store(Request $request)
    {
        $group  = $this->item()->create($request->all());
        //TEMP
        $group->grouplists()->create([...$request->grouplist, 'season_id' => $request->grouplist['season']['id']]);

        return new GroupResource($group);
    }

    public function show($id)
    {
        return new GroupResource($this->item()->whereId($id)->with(['grouplist.season', 'language'])->firstOrFail());
    }

    public function update(Request $request, $id)
    {
        $group =  $this->item()->whereId($id)->firstOrFail();

        $group->grouplist()->firstOrFail()->update([...$request->grouplist, 'season_id' => $request->grouplist['season']['id']]);

        $group->update($request->all());

        return new GroupResource($group);
    }

    public function destroy($id)
    {
        return response()->json(
            ['deleted' => (bool)$this->item()->whereId($id)->delete()]
        );
    }
}
