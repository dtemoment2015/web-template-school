<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Api\HestiaCP;
use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\AddDomainRequest;
use App\Http\Resources\v1\Admin\DomainResource;
use App\Jobs\v1\addDomainJob;
use Illuminate\Http\Request;

class BusinessDomainController extends Controller
{

    public function __construct()
    {
        parent::__construct('domains');
    }

    public function index()
    {
        return DomainResource::collection($this->item()->paginate());
    }

    public function store(AddDomainRequest $request)
    {
        // if ($error = HestiaCP::vAddDnsDomain($request->name, false)) {
        //     return response()->json(['message' => __('Попробуйте еще раз. ') . $error], 422);
        // }
        $domainName = trim($request->name);

        $domainName =  str_replace('https://', "", $domainName);
        $domainName =  str_replace('http://', "", $domainName);
        $domainName =  str_replace('www.', "", $domainName);
        
        if (strpos($domainName, 'edutoday.uz') || $domainName == 'edutoday.uz') {
            return response()->json(['message' => __('EDUTODAY RESERVED')], 422);
        }
        
        $domain  = $this->item()->create($request->all());

        addDomainJob::dispatch($domain)->onQueue('domain');

        return new DomainResource($domain);
    }

    public function show($id)
    {
        return new DomainResource($this->item()->whereId($id)->firstOrFail());
    }

    public function update(Request $request, $id)
    {
        $domain =  $this->item()->whereId($id)->firstOrFail();
        exec("/usr/local/hestia/bin/v-add-letsencrypt-domain denis " . $domain->name . " www." . $domain->name . "", $info, $result);
        if ($result == 0) {
            exec("/usr/local/hestia/bin/v-add-web-domain-ssl-force denis " . $domain->name . "");
            $domain->is_active = 1;
            $domain->save();
        } else {
            return response()->json(['message' => __('SSL ERROR')], 422);
        }

        return new DomainResource($domain);
    }

    public function destroy($id)
    {
        return response()->json(
            ['deleted' => (bool)$this->item()->whereId($id)->delete()]
        );
    }
}
