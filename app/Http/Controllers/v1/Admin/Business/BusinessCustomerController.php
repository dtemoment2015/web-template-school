<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\CustomerResource;
use App\Models\Customer;
use Illuminate\Http\Request;

class BusinessCustomerController extends Controller
{

    public function __construct()
    {
        parent::__construct('customers');
    }

    public function index()
    {
        return CustomerResource::collection($this->item()
            ->when($search = request('search'), function ($query) use ($search) {
                $query->search($search)
                    ->orderBy('relevance', 'DESC');
            })
            ->with('positions')
            ->orderBy('last_name')
            ->orderBy('first_name')
            ->groupBy('customers.id')
            ->paginate()
        );
    }

    public function store(Request $request)
    {
        $item = Customer::firstOrCreate(
            ['phone' => $request->phone],
            $request->all()
        );

        $request->_business->customers()->syncWithoutDetaching(
            [
                $item->id => [
                    'is_owner' => false
                ]
            ]
        );

        $item->positions()->sync(
            collect((array) optional($request)->positions)->mapWithKeys(function ($item) use ($request) {
                return [$item => ['business_id' =>  $request->get('_business')->id]];
            })
        );


        return new CustomerResource(
            $item
        );
    }

    public function show($id)
    {
        return new CustomerResource($this->item()->with('positions')->whereId($id)->firstOrFail());
    }

    public function update(Request $request, $id)
    {
        $item =  $this->item()->whereId($id)->firstOrFail();
        $item->update($request->all());
        $item->positions()->sync(
            collect((array) optional($request)->positions)->mapWithKeys(function ($item) use ($request) {
                return [$item => ['business_id' =>  $request->get('_business')->id]];
            })
        );

        return new CustomerResource($item);
    }

    public function destroy(Request $request, $id)
    {
        return response()->json(
            [
                'deleted' => $request->_business->customers()->wherePivot('is_owner', 0)->detach($id)
            ]
        );
    }
}
