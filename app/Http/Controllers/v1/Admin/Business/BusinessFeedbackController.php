<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\FeedbackResource;
use Illuminate\Http\Request;

class BusinessFeedbackController extends Controller
{
    public function __construct()
    {
        parent::__construct('feedback');
    }

    public function index()
    {
        return FeedbackResource::collection($this->item()->paginate());
    }

    public function store(Request $request)
    {
        return new FeedbackResource($this->item()->create($request->all()));
    }

    public function show($id)
    {
        return new FeedbackResource($this->item()->whereId($id)->firstOrFail());
    }

    public function update(Request $request, $id)
    {
        $customer =  $this->item()->whereId($id)->firstOrFail();
        $customer->update($request->all());
        return new FeedbackResource($customer);
    }

    public function destroy($id)
    {
        return response()->json(
            ['deleted' => (bool)$this->item()->whereId($id)->delete()]
        );
    }
}
