<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\StudentResource;
use App\Models\Student;
use Illuminate\Http\Request;

class BusinessGroupStudentController extends Controller
{
    public function __construct()
    {
        parent::__construct('groups');
    }

    public function index($groupId)
    {
        return StudentResource::collection(
            $this->item()
                ->whereId($groupId)
                ->firstOrFail()
                ->students()
                ->orderBy('last_name')
                ->orderBy('first_name')
                ->wherePivot('left_at', null)
                ->paginate()
        );
    }

    public function store(Request $request, $groupId)
    {

        $student = Student::findOrFail(optional($request)->student_id);

        if ($student->groups()->wherePivot('left_at', null)->exists()) {
            return response()->json(['message' => __('Ученик уже числится в этом или другом классе')], 422);
        }

        $this
            ->item()
            ->whereId($groupId)
            ->firstOrFail()
            ->students()
            ->attach(
                [$student->id => [
                    'join_at' => now(),
                ]]
            );

        return response()->json(['data' => [
            'id' => true
        ]]);
    }

    public function destroy($groupId, $id)
    {
        return response()->json(
            [
                'deleted' =>  $this
                    ->item()
                    ->whereId($groupId)
                    ->firstOrFail()
                    ->students()
                    ->wherePivot('left_at', null)
                    ->syncWithoutDetaching(
                        [$id => [
                            'left_at' => now()
                        ]]
                    )
            ]
        );
    }
}
