<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\FormResource;
use Illuminate\Http\Request;

class BusinessFormController extends Controller
{

    public function __construct()
    {
        parent::__construct('forms');
    }

    public function index()
    {
        return FormResource::collection($this->item()->paginate());
    }

    public function store(Request $request)
    {
        return new FormResource($this->item()->create($request->all()));
    }

    public function show($id)
    {
        return new FormResource($this->item()->whereId($id)->firstOrFail());
    }

    public function update(Request $request, $id)
    {
        $customer =  $this->item()->whereId($id)->firstOrFail();
        $customer->update($request->all());

        return new FormResource($customer);
    }

    public function destroy($id)
    {
        return response()->json(
            ['deleted' => (bool)$this->item()->whereId($id)->delete()]
        );
    }
}
