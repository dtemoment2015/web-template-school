<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\AddPostRequest;
use App\Http\Resources\v1\Admin\PostResource;
use App\Models\Project\Rubric;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BusinessPostController extends Controller
{

    public function __construct()
    {
        parent::__construct('posts');
    }

    public function index()
    {
        return PostResource::collection(
            $this->item()
                ->when($search = request('search'), function ($query) use ($search) {
                    $query->search($search)
                        ->orderBy('relevance', 'DESC');
                })
                ->when(request('rubric_code'), function ($query) {
                    $query->whereHas('rubrics', function ($q) {
                        $q->whereCode(request('rubric_code'));
                    });
                })
                ->with(['season', 'grouplists', 'group', 'weeks'])
                ->orderByDesc('publication_at')
                ->orderByDesc('id')
                ->groupBy('posts.id')
                ->paginate()
        );
    }

    public function store(AddPostRequest $request)
    {
        $item = $this->item()->create($request->all());
        if ($rubric_code = optional($request)->rubric_code) {
            $item->rubrics()->sync(
                [Rubric::whereCode($rubric_code)->firstOrFail()->id]
            );
        }

        $item->grouplists()->sync((array)  optional($request)->grouplists);

        $item->weeks()->sync((array)  optional($request)->weeks);

        if (($images = optional($request)->images) &&
            is_array($images) &&
            count($images)
        ) {
            foreach ($images as $image) {
                $item->addMedia($image)->toMediaCollection('posts');
            }
        }

        $item->code = Str::of($item->title . time() . ' ' . $item->id)->slug('-');

        $item->publication_at = now();
        $item->is_active = 0;
        $item->save();

        return new PostResource($item);
    }

    public function show($id)
    {
        return new PostResource($this->item()->with(['season', 'grouplists', 'group.grouplist', 'media', 'weeks'])->whereId($id)->firstOrFail());
    }

    public function update(AddPostRequest $request, $id)
    {
        $item =  $this->item()->whereId($id)->firstOrFail();

        $item->update($request->all());
        if (($images = optional($request)->images) &&
            is_array($images) &&
            count($images)
        ) {
            foreach ($images as $image) {
                $item->addMedia($image)->toMediaCollection('posts');
            }
        }

        $item->weeks()->sync((array)  optional($request)->weeks);

        $item->grouplists()->sync((array)  optional($request)->grouplists);

        return new PostResource($item->load('media'));
    }

    public function destroy($id)
    {
        return response()->json(
            ['deleted' => (bool)$this->item()->whereId($id)->delete()]
        );
    }
}
