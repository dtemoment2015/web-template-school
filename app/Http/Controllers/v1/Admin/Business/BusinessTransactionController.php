<?php

namespace App\Http\Controllers\v1\Admin\Business;

use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentRequest;
use App\Http\Requests\v1\GenerateTransferOrder;
use App\Http\Requests\v1\GenerateTransferOrderRequest;
use App\Http\Resources\v1\Admin\TransactionResource;
use App\Models\Project\Tariff;
use Exception;
use Illuminate\Http\Request;

class BusinessTransactionController extends Controller
{
    public function __construct()
    {
        parent::__construct('transactions');
    }

    public function index()
    {
        return TransactionResource::collection($this->item()
            ->with(['tariff', 'status', 'business', 'payment'])
            // ->orderByDesc('till_at')
            ->orderByDesc('id')
            ->paginate());
    }

    public function store(PaymentRequest $request)
    {
        $tariff = Tariff::whereId($request->tariff_id)->limit(1)->whereIsActive(true)->firstOrFail();
        $transaction = $this->item();
        $check =   $transaction->create(
            [
                'price' => $tariff->price,
                'is_paid' => 0,
                'is_active' => 1,
                'tariff_id' => $tariff->id,
                'currency' => $tariff->currency,
                'state' => 2,
            ]
        );

        return new TransactionResource($check);
    }

    public function show($id)
    {
        return new TransactionResource($this->item()->with('tariff')->whereId($id)->firstOrFail());
    }

    public function update(GenerateTransferOrderRequest $request, $id)
    {
        $transaction =  $this->item()->whereId($id)->firstOrFail();

        $transaction->update($request->all());
        try {
            $pdf = PDF::loadView('order', ['data' => $transaction]);
            $path = storage_path('app/public/orders');
            $fileName =  $transaction->id . '.' . 'pdf';
            $pdf->save($path . '/' . $fileName);
        } catch (Exception $e) {
            return response()->json(['message' => 'TRY AGAIN PLEASE'], 422);
        }

        return new TransactionResource($transaction);
    }

    public function destroy($id)
    {
        return response()->json(
            ['deleted' => (bool)$this->item()->whereId($id)->delete()]
        );
    }
}
