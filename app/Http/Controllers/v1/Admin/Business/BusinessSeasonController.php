<?php

namespace App\Http\Controllers\v1\Admin\Business;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\SeasonResource;
use Illuminate\Http\Request;

class BusinessSeasonController extends Controller
{

    public function __construct()
    {
        parent::__construct('seasons');
    }

    
    public function index()
    {
        return SeasonResource::collection($this->item()->paginate());
    }

    public function store(Request $request)
    {
        return new SeasonResource($this->item()->create($request->all()));
    }

    public function show($id)
    {
        return new SeasonResource($this->item()->whereId($id)->firstOrFail());
    }

    public function update(Request $request, $id)
    {
        $customer =  $this->item()->whereId($id)->firstOrFail();
        $customer->update($request->all());
        return new SeasonResource($customer);
    }

    public function destroy($id)
    {
        return response()->json(
            ['deleted' => (bool)$this->item()->whereId($id)->delete()]
        );
    }
}
