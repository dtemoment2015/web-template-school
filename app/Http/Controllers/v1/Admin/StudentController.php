<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\StudentResource;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index(Student $student)
    {
        return StudentResource::collection($student->paginate());
    }

    public function store(Request $request, Student $student)
    {
        return new StudentResource($student->create($request->all()));
    }

    public function show(Student $student)
    {
        return new StudentResource($student);
    }

    public function update(Request $request, Student $student)
    {
        $student->update($request->all());
        return new StudentResource($student);
    }

    public function destroy(Student $student)
    {
        return response()->json(
            ['deleted' => (bool)$student->delete()]
        );
    }
}
