<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\FeatureCollection;
use App\Http\Resources\v1\Admin\LanguageResource;
use App\Http\Resources\v1\Admin\LocationGlobalCollect;
use App\Http\Resources\v1\Admin\TextResource;
use App\Models\Project\Feature;
use App\Models\Project\Language;
use App\Models\Project\Location;
use App\Models\Project\Text;
use Illuminate\Support\Facades\Cache;

class SystemController extends Controller
{

    public function index()
    {
        //Cache::flush();

        $appLocale = app()->getLocale();

        return response()->json(
            [
                'locations' => Cache::rememberForever('locations_' . $appLocale, function () {
                    return (new LocationGlobalCollect(Location::with('childs')->whereDoesntHave('parent')->get()))->jsonSerialize();
                }),
                'text' =>  Cache::rememberForever('text_' . $appLocale, function () {
                    return TextResource::collection(Text::all())->collection->pluck('value', 'key')->jsonSerialize();
                }),
                'localizations' => Cache::rememberForever('localizations_' . $appLocale, function () {
                    return  LanguageResource::collection(Language::orderBy('code')->whereIsActive(true)->get())->jsonSerialize();
                }),
                'features' => Cache::rememberForever('features_' . $appLocale, function () {
                    return (new FeatureCollection(Feature::whereIsActive(true)->get()))->jsonSerialize();
                })
            ]
        );
    }
}
