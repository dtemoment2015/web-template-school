<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\FieldResource;
use App\Models\Form;
use Illuminate\Http\Request;

class FormFieldController extends Controller
{
    public function index(Form $Form)
    {
        return FieldResource::collection($Form->fields()->paginate());
    }

    public function store(Request $request, Form $Form)
    {
        return new FieldResource($Form->fields()->create($request->all()));
    }

    public function show(Form $Form, $id)
    {
        return new FieldResource($Form->fields()->whereId($id)->firstOrFail());
    }

    public function update(Request $request, Form $Form, $id)
    {
        $item =  $Form->fields()->whereId($id)->firstOrFail();

        $item->update($request->all());

        return new FieldResource($item);
    }


    public function destroy(Form $Form, $id)
    {
        return response()->json(
            ['deleted' => (bool)$Form->fields()->whereId($id)->delete()]
        );
    }
}
