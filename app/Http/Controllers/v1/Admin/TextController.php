<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\TextResource;
use App\Models\Project\Text;
use Illuminate\Http\Request;

class TextController extends Controller
{
    public function index(Text $text)
    {
        return TextResource::collection(
            $text->when($search = request('search'), function ($query) use ($search) {
                $query->search($search)
                    ->orderBy('relevance', 'DESC');
            })
                ->orderByDesc('id')
                ->groupBy('texts.id')
                ->paginate()
        );
    }

    public function store(Request $request, Text $text)
    {
        return new TextResource($text->create($request->all()));
    }

    public function show(Text $text)
    {
        return new TextResource($text);
    }

    public function update(Request $request, Text $text)
    {
        $text->update($request->all());
        return new TextResource($text);
    }

    public function destroy(Text $text)
    {
        return response()->json(
            ['deleted' => (bool)$text->delete()]
        );
    }
}
