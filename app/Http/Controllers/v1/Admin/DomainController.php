<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\DomainResource;
use App\Models\Business\Domain;
use Illuminate\Http\Request;

class DomainController extends Controller
{
    public function index(Domain $Domain)
    {
        return DomainResource::collection($Domain->with('business')->paginate());
    }

    public function store(Request $request, Domain $Domain)
    {
        return new DomainResource($Domain->create($request->all()));
    }

    public function show(Domain $Domain)
    {
        return new DomainResource($Domain->load('business'));
    }

    public function update(Request $request, Domain $Domain)
    {
        $Domain->update($request->all());

        return new DomainResource($Domain);
    }

    public function destroy(Domain $Domain)
    {
        return response()->json(
            ['deleted' => (bool)$Domain->delete()]
        );
    }
}
