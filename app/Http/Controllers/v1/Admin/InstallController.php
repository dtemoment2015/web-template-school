<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\BusinessAccountResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class InstallController extends Controller
{
    public function store(Request $request)
    {
        $business = $request->get('_business');
        $business->update($request->all());
        if (isset($request->icon)) {
            $icon = (array) optional($request)->icon;
            $business->features()->sync((array) $icon);
        }
        if (isset($request->text)) {
            foreach ($request->text as $t) {
                $business->values()->updateOrCreate(
                    ['feature_id' => $t['feature_id']],
                    [
                        'feature_id' => $t['feature_id'],
                        'value' => $t['value'],
                        'code' => $t['code'],
                    ],
                );
            }
        }

        if (($media = request('images')) && is_array($media)
        ) {
            foreach ($media as $image) {
                $business->addMedia($image)->toMediaCollection('albums');
            }
        }
        return new BusinessAccountResource($business->load('media'));
    }
}
