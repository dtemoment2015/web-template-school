<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\BusinessResource;
use App\Models\Business;
use Illuminate\Http\Request;

class BusinessController extends Controller
{
    public function index(Business $business)
    {
        return BusinessResource::collection(
            $business->with(['customers', 'domains'])
                ->orderByDesc('id')
                ->paginate()
        );
    }

    public function store(Request $request, Business $business)
    {
        return new BusinessResource($business->create($request->all()));
    }

    public function show(Business $business)
    {
        return new BusinessResource($business);
    }

    public function update(Request $request, Business $business)
    {
        $business->update($request->all());
        return new BusinessResource($business);
    }

    public function destroy(Business $business)
    {
        return response()->json(
            ['deleted' => (bool)$business->delete()]
        );
    }
}
