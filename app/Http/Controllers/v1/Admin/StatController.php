<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Models\Business;
use App\Models\Business\Album;
use App\Models\Business\Feedback;
use App\Models\Business\Graduate;
use App\Models\Business\Group;
use App\Models\Business\Post;
use App\Models\Business\Season;
use App\Models\Business\Transaction;
use App\Models\Customer;
use App\Models\Student;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class StatController extends Controller
{
    public function index()
    {
        return response()->json(
            [
                [
                    'key' => 'businesses',
                    'title' => 'Всего сайтов',
                    'icon' => 'ti-shopping-cart-full',
                    'value' => Business::count()
                ],
                [
                    'key' => 'graduates',
                    'title' => 'Активные сайты',
                    'icon' => 'ti-id-badge',
                    'value' => Business::whereIsActive(true)->whereIsProduction(true)->count()
                ],
                [
                    'key' => 'feedback',
                    'title' => 'Всего получено на сумму',
                    'icon' => 'ti-id-badge',
                    'value' => Transaction::whereIsPaid(true)->sum('price')
                ],
                [
                    'key' => 'customers',
                    'title' => 'Всего пользователей',
                    'icon' => 'ti-id-badge',
                    'value' => Customer::count()
                ],
                [
                    'key' => 'customers',
                    'title' => 'Всего создано постов',
                    'icon' => 'ti-id-badge',
                    'value' => Post::count()
                ],
                [
                    'key' => 'customers',
                    'title' => 'Общее число обращений',
                    'icon' => 'ti-id-badge',
                    'value' => Feedback::count()
                ],
                [
                    'key' => 'customers',
                    'title' => 'Общее число учебных сезонов',
                    'icon' => 'ti-id-badge',
                    'value' => Season::count()
                ],
                [
                    'key' => 'customers',
                    'title' => 'Общее число выпускных групп',
                    'icon' => 'ti-id-badge',
                    'value' => Graduate::count()
                ],
                [
                    'key' => 'customers',
                    'title' => 'Общее число  групп',
                    'icon' => 'ti-id-badge',
                    'value' => Group::count()
                ],
                [
                    'key' => 'customers',
                    'title' => 'Общее число учащихся',
                    'icon' => 'ti-id-badge',
                    'value' => Student::count()
                ],
                [
                    'key' => 'customers',
                    'title' => 'Общее число альбомов',
                    'icon' => 'ti-id-badge',
                    'value' => Album::count()
                ],
                [
                    'key' => 'customers',
                    'title' => 'Общее число загруженных файлов',
                    'icon' => 'ti-id-badge',
                    'value' => Media::count()
                ],
                [
                    'key' => 'customers',
                    'title' => 'Использование диска медиа (МБ)',
                    'icon' => 'ti-id-badge',
                    'value' => round(Media::sum('size') / 1024 / 8)
                ],
            ]
        );
    }
}
