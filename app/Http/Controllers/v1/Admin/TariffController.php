<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\TariffResource;
use App\Models\Project\Tariff;
use Illuminate\Http\Request;

class TariffController extends Controller
{
    public function index(Tariff $tariff)
    {
        return TariffResource::collection(
            $tariff->when(request('is_prod'), function ($query) {
                    $query->whereIsActive(true)->where('price', '>', 0);
                })
                ->orderBy('price')
                ->paginate()
        );
    }

    public function store(Request $request, Tariff $tariff)
    {

        
        return new TariffResource($tariff->create($request->all()));
    }

    public function show(Tariff $tariff)
    {
        return new TariffResource($tariff);
    }

    public function update(Request $request, Tariff $tariff)
    {
        $tariff->update($request->all());

        return new TariffResource($tariff);
    }

    public function destroy(Tariff $tariff)
    {
        return response()->json(
            ['deleted' => (bool)$tariff->delete()]
        );
    }
}
