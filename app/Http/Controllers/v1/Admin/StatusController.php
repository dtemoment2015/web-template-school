<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\StatusResource;
use App\Models\Project\Status;
use Illuminate\Http\Request;

class StatusController extends Controller
{

    public function index(Status $status)
    {
        return StatusResource::collection($status->when(request('type'), function ($query) {
            $query->whereType(request('type'));
        })
    
        ->paginate());
    }

    public function store(Request $request, Status $status)
    {
        return new StatusResource($status->create($request->all()));
    }

    public function show(Status $status)
    {
        return new StatusResource($status);
    }

    public function update(Request $request, Status $status)
    {
        $status->update($request->all());
        return new StatusResource($status);
    }

    public function destroy(Status $status)
    {
        return response()->json(
            ['deleted' => (bool)$status->delete()]
        );
    }
}
