<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\TransactionResource;
use App\Models\Business\Transaction;
use App\Models\Project\Tariff;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index(Transaction $transaction)
    {
        return TransactionResource::collection($transaction
            ->with(['tariff', 'status', 'business', 'payment'])
            ->orderByDesc('id')
            ->paginate());
    }

    public function store(Request $request, Transaction $transaction)
    {
        $tariff = Tariff::whereId($request->tariff_id)->firstOrFail();

        $input = $request->all();
        $input['price'] = $tariff->price;
        $input['currency'] = $tariff->currency;
        if ($input['is_paid']) {
            $now = now();
            $input['from_at'] = $now;
            $input['paid_at'] = $now->clone()->startOfDay();
            $input['till_at'] = $now->clone()->addDays($tariff->period)->endOfDay();
        }

        return new TransactionResource($transaction->create($input));
    }

    public function show(Transaction $transaction)
    {
        return new TransactionResource($transaction->load(['tariff', 'status', 'business', 'payment']));
    }

    public function update(Request $request, Transaction $transaction)
    {
        $tariff = Tariff::whereId($request->tariff_id)->firstOrFail();
        $input = $request->all();
        $input['price'] = $tariff->price;
        $input['currency'] = $tariff->currency;
        if (!$transaction->is_paid && $input['is_paid']) {
            $now = now();
            $input['from_at'] = $now;
            $input['paid_at'] = $now->clone()->startOfDay();
            $input['till_at'] = $now->clone()->addDays($tariff->period)->endOfDay();
        }
        $transaction->update($input);

        return new TransactionResource($transaction);
    }

    public function destroy(Transaction $transaction)
    {
        return response()->json(
            ['deleted' => (bool)$transaction->delete()]
        );
    }
}
