<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\RubricResource;
use App\Models\Project\Rubric;
use Illuminate\Http\Request;

class RubricController extends Controller
{
    public function index(Rubric $rubric)
    {
        return RubricResource::collection($rubric->paginate());
    }

    public function store(Request $request, Rubric $rubric)
    {
        return new RubricResource($rubric->create($request->all()));
    }

    public function show(Rubric $rubric)
    {
        return new RubricResource($rubric);
    }

    public function update(Request $request, Rubric $rubric)
    {
        $rubric->update($request->all());
        return new RubricResource($rubric);
    }

    public function destroy(Rubric $rubric)
    {
        return response()->json(
            ['deleted' => (bool)$rubric->delete()]
        );
    }
}
