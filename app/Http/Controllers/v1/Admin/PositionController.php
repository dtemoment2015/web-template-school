<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\PositionResource;
use App\Models\Project\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{

    public function index(Position $position)
    {
        return PositionResource::collection($position
            ->when($search = request('search'), function ($query) use ($search) {
                $query->search($search)
                    ->orderBy('relevance', 'DESC');
            })
            ->orderByTranslation('title')
            ->groupBy('positions.id')
            ->paginate());
    }

    public function store(Request $request, Position $position)
    {
        return new PositionResource($position->create($request->all()));
    }

    public function show(Position $position)
    {
        return new PositionResource($position);
    }

    public function update(Request $request, Position $position)
    {
        $position->update($request->all());
        return new PositionResource($position);
    }

    public function destroy(Position $position)
    {
        return response()->json(
            ['deleted' => (bool)$position->delete()]
        );
    }
}
