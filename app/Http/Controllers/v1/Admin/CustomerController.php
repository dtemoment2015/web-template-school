<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Admin\CustomerResource;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index(Customer $customer)
    {
        return CustomerResource::collection($customer->paginate());
    }

    public function store(Request $request, Customer $customer)
    {
        return new CustomerResource($customer->create($request->all()));
    }

    public function show(Customer $customer)
    {
        return new CustomerResource($customer);
    }

    public function update(Request $request, Customer $customer)
    {
        $customer->update($request->all());

        return new CustomerResource($customer);
    }

    public function destroy(Customer $customer)
    {
        return response()->json(
            ['deleted' => (bool)$customer->delete()]
        );
    }
}
