<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Admin\Auth\CodeRequest;
use App\Http\Requests\v1\Admin\Auth\LoginRequest;
use App\Http\Resources\v1\Admin\CustomerResource;
use App\Http\Resources\v1\Admin\CustomerAccountResource;
use App\Jobs\v1\SmsJob;
use App\Models\Customer;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function getCode(
        CodeRequest $request,
        Customer $customer
    ) {
        //CREATE OR FIND CUSTOMER IN BASE
        if ($user = $customer->firstOrCreate(
            ['phone' => $request->phone],
        )) {
            //IF CODE NO EXPIRED - RETURN ERRO
            if ($user->code_expired_at > now()) {
                return response()->json([
                    'message' =>  __(
                        'message.phone.repeat_request',
                        ['date_at' => $user->code_expired_at]
                    ),
                ], 422);
            }

            $code = rand(7777, 9999);

            if ($request->phone == '+998903467899') {
                $code = '11991';
            }

            $user->code_expired_at = now()
                ->addSeconds($customer::CODE_EXPIRED_SECOND);
            $user->code = $code;
            $user->save();

            //3. SEND SMS
            SmsJob::dispatch(
                $user->phone,
                __('message.sms.auth', ['code' =>  $code])
            )
                ->onQueue('sms');
        }

        return new CustomerResource($user);
    }

    public function doLogIn(
        LoginRequest $request,
        Customer $customer
    ) {
        //GET USER
        $user = $customer->wherePhone(
            $request->phone
        )->first();

        //CHECK CODE AND LIFETIME CODE
        if (
            !Hash::check($request->code, $user->code)
            || $user->code_expired_at < now()
        ) {

            return response()->json([
                'message' =>  __(
                    'message.code.wrong'
                ),
            ], 422);
        }

        //SET CURRENT CODE EXPIRED DATE
        $user->code_expired_at = now();
        $user->save();

        //GET ACCESS TOKEN
        $token = $user->createToken('CUSTOMER');

        return (new CustomerAccountResource($user))
            ->response()
            ->header(
                'X-Access-Token',
                optional($token)->accessToken
            )
            ->header(
                'X-Expires-At',
                optional(optional($token)->token)['expires_at']
            );
    }
}
