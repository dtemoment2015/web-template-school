<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $businessModel;

    public function __construct($businessModel = null)
    {
        $this->businessModel = $businessModel;
    }

    //Перевести это все в REPOSITORY PATTERN

    public function item()
    {
        $model = $this->businessModel;

        return  request()->get('_business')->$model();
    }
    public function uploadMedia($item = null)
    {

        if ($images_delete =  request('media_destroy')) {
            foreach ($images_delete as $image) {
                try {
                    $item->deleteMedia($image);
                } catch (Exception $e) {
                }
            }
        }

        if (($media = request('media')) && is_array($media)
        ) {
            foreach ($media as $image) {
                $item->addMedia($image)->toMediaCollection('albums');
            }
        }
    }
    //Перевести это все в REPOSITORY PATTERN

}
