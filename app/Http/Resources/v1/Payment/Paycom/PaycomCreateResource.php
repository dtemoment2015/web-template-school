<?php

namespace App\Http\Resources\v1\Payment\Paycom;

use Illuminate\Http\Resources\Json\JsonResource;

class PaycomCreateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'create_time' => (int) $this->created_at->timestamp * 1000,
            'transaction' => (string) $this->uuid,
            'state' =>  (int)  $this->state,
        ];
    }
}
