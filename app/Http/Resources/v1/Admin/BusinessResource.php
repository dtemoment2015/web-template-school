<?php

namespace App\Http\Resources\v1\Admin;


use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Business
 */
class BusinessResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
    //     return [
    //         'id' => $this->id,
    //         'course_count' => 2
    // ];
         return parent::toArray($request);
    }
}
