<?php

namespace App\Http\Resources\v1\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class GroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'language_id' => $this->language_id,
            'language' => new LanguageResource($this->language),
            'grouplist_id' => $this->grouplist_id,
            'grouplist' => new GrouplistResource($this->whenLoaded('grouplist')),
            'grouplists' =>  GrouplistResource::collection($this->whenLoaded('grouplists')),
            'name' =>  $this->whenLoaded('grouplist', function(){
                return $this->grouplist->course .' '. $this->grouplist->title;
            }),
            'business_id' => $this->business_id,
            'business' =>  new BusinessResource($this->whenLoaded('business')),
        ];
    }
}
