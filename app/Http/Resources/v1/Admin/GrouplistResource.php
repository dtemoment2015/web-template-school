<?php

namespace App\Http\Resources\v1\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class GrouplistResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'course' => $this->course,
            'title' => $this->title,
            'group_id' => $this->group_id,
            'group' =>  new GroupResource($this->whenLoaded('group')),
            'season_id' => $this->season_id,
            'season' => new SeasonResource($this->whenLoaded('season')),
            'name' =>  (string) optional($this)->course . ' ' . (string) optional($this)->title,
        ];
    }
}
