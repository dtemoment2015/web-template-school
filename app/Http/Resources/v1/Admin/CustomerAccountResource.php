<?php

namespace App\Http\Resources\v1\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerAccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'phone' => $this->phone,
            'last_name' => $this->last_name,
            'first_name' => $this->first_name,
            'family_name' => $this->family_name,
            'is_root' => $this->is_root,
            'root' => $this->when($request->header('root', null) && $this->is_root, function () {
                return true;
            }),
            'PINFL' => $this->PINFL,
            'business' => $this->when($bid = (int) $request->header('bid', null), function () use ($bid) {
                return new BusinessAccountResource($this->businesses()->with('media')->whereId($bid)->first());
            }),
            'routes' => [
                [
                    'icon' => 'ti-shift-left',
                    'title' => 'Сменить бизнес',
                    'path' => 'business_change',
                    'key' => 'business_change',
                ],
                [
                    'icon' => '',
                    'title' => 'Главная',
                    'path' => 'dashboard',
                    'key' => 'dashboard',
                ],
                [
                    'icon' => '',
                    'title' => 'Сайты',
                    'path' => 'businesses',
                    'key' => 'businesses',
                ],
                [
                    'icon' => '',
                    'title' => 'Посты',
                    'path' => 'posts',
                    'key' => 'posts',
                ],
                [
                    'icon' => '',
                    'title' => 'Рубрики',
                    'path' => 'rubrics',
                    'key' => 'rubrics',
                ],
                [
                    'icon' => '',
                    'title' => 'Пользователи',
                    'path' => 'customers',
                    'key' => 'customers',
                ],
                [
                    'icon' => '',
                    'title' => 'Методы оплат',
                    'path' => 'payments',
                    'key' => 'payments',
                ],
                [
                    'icon' => '',
                    'title' => 'Статусы',
                    'path' => 'statuses',
                    'key' => 'statuses',
                ],
                [
                    'icon' => '',
                    'title' => 'Студенты',
                    'path' => 'students',
                    'key' => 'students',
                ],
                [
                    'icon' => '',
                    'title' => 'Должности',
                    'path' => 'positions',
                    'key' => 'positions',
                ],
                [
                    'icon' => '',
                    'title' => 'Тариффы',
                    'path' => 'tariffs',
                    'key' => 'tariffs',
                ],

                [
                    'icon' => '',
                    'title' => 'Системные слова',
                    'path' => 'texts',
                    'key' => 'texts',
                ],
                [
                    'icon' => '',
                    'title' => 'Глобальные формы',
                    'path' => 'forms',
                    'key' => 'forms',
                ],
                [
                    'icon' => '',
                    'title' => 'Оплаты',
                    'path' => 'transactions',
                    'key' => 'transactions',
                ],
                [
                    'icon' => '',
                    'title' => 'Языки',
                    'path' => 'languages',
                    'key' => 'languages',
                ],
                [
                    'icon' => '',
                    'title' => 'Характеристики',
                    'path' => 'features',
                    'key' => 'features',
                ],
                [
                    'icon' => '',
                    'title' => 'Домены',
                    'path' => 'domains',
                    'key' => 'domains',
                ],
                [
                    'icon' => '',
                    'title' => 'Выход из системы',
                    'path' => 'quit',
                    'key' => 'quit',
                ],
            ],
        ];
    }
}
