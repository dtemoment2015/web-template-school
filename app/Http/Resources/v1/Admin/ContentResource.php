<?php

namespace App\Http\Resources\v1\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ContentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'post_id' => $this->post_id,
            'type' => $this->type,
            'content_type' => $this->content_type,
            'content_id' => $this->content_id,
            'body' => $this->body,
            'value' => $this->value,
            'description' => $this->description,
            'translations' => $this->translations,
            'content' => $this->when(
                ($this->content_type),
                function () {
                    switch ($this->content_type) {
                        case ('form'): {
                                return new FormResource($this->content->load('fields'));
                                break;
                            }
                        case ('post'): {
                                return new PostResource($this->content->load(['media', 'season', 'grouplists', 'group.grouplist']));
                                break;
                            }
                    }
                }
            ),
        ];
    }
}
