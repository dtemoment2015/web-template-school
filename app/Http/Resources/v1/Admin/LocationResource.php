<?php

namespace App\Http\Resources\v1\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class LocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'translations' => $this->translations,
            'name' => $this->name,
            'address' => $this->address,
            'parent' => $this->whenLoaded('parent'),
            'childs' => $this->whenLoaded('childs'),
        ];
    }
}
