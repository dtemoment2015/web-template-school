<?php

namespace App\Http\Resources\v1\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'is_active' => $this->is_active,
            'code' => $this->code,
            'title' => $this->title,
            'description' => $this->description,
            'publication_at' => $this->publication_at,
            'created_at' => $this->created_at,
            'week_number' => $this->week_number,
            'position' => $this->position,
            'time_from' => $this->time_from,
            'time_till' => $this->time_till,
            'body' => $this->body,
            'grouplists' => GrouplistResource::collection($this->whenLoaded('grouplists')),
            'rubrics' => RubricResource::collection($this->whenLoaded('rubrics')),
            'season' => new SeasonResource($this->whenLoaded('season')),
            'business' => new BusinessResource($this->whenLoaded('business')),
            'group' => new GroupResource($this->whenLoaded('group')),
            'media' => MediaResource::collection($this->whenLoaded('media')),
            'contents' => ContentResource::collection($this->whenLoaded('contents')),
            'weeks' => StatusResource::collection($this->whenLoaded('weeks')),
            'business_id' => $this->business_id,
            'group_id' => $this->group_id,
            'season_id' => $this->season_id,
            'translations' => $this->translations,
            'preview' => $this->preview,
        ];
    }
}
