<?php

namespace App\Http\Resources\v1\Admin;

use App\Models\Project\Feature;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class BusinessAccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $languages =  $this->languages;
        $values = $this->values->pluck(null, 'code');
        return [
            'id' => $this->id,
            'code' => $this->code,
            'is_install' => (int) $this->is_install,
            'title' => $this->title,
            'logo' => $this->logo,
            'transaction' => new  TransactionResource(optional($this->transaction)->load('tariff')),
            'done_dns' => $this->done_dns,
            'done_domain' => $this->done_domain,
            'done_alias' => $this->done_alias,
            'done_redirect' => $this->done_redirect,
            'done_ssl' => $this->done_ssl,
            'is_production' => $this->is_production,
            'name' => $this->name,
            'title' => $this->title,
            'description' => $this->description,
            'translations' => $this->translations,
            'media' => MediaResource::collection($this->whenLoaded('media')),
            'routes' => [
                [
                    'icon' => 'ti-arrow-circle-left',
                    'title' => 'Мои сайты',
                    'path' => 'business_change',
                    'key' => 'business_change',
                ],
                [
                    'icon' => 'ti-home',
                    'title' => 'Главная',
                    'path' => 'dashboard',
                    'key' => 'dashboard',
                ],
                [
                    'icon' => 'ti-gallery',
                    'title' => 'Контент сайта',
                    'path' => 'contents/news',
                    'key' => 'contents',
                ],
                [
                    'icon' => 'ti-location-pin',
                    'title' => 'Адреса завдений',
                    'path' => 'addresses',
                    'key' => 'addresses',
                ],
                [
                    'icon' => 'ti-email',
                    'title' => 'Сообщения с сайта',
                    'path' => 'feedback',
                    'key' => 'feedback',

                ],
                [
                    'icon' => 'ti-id-badge',
                    'title' => 'Сотрудники',
                    'path' => 'business_customers',
                    'key' => 'business_customers',

                ],
                [
                    'icon' => 'ti-archive',
                    'title' => 'Учебные сезоны',
                    'path' => 'seasons',
                    'key' => 'seasons',
                ],
                [
                    'icon' => 'ti-agenda',
                    'title' => 'Группы',
                    'path' => 'groups',
                    'key' => 'groups',
                ],
                [
                    'icon' => 'ti-user',
                    'title' => 'Ученики',
                    'path' => 'business_students',
                    'key' => 'business_students',
                ],
                [
                    'icon' => 'ti-world',
                    'title' => 'Домены',
                    'path' => 'business_domains',
                    'key' => 'business_domains',
                ],
                [
                    'icon' => 'ti-receipt',
                    'title' => 'Оплаты',
                    'path' => 'business_transactions',
                    'key' => 'business_transactions',
                ],
                [
                    'icon' => 'ti-power-off',
                    'title' => 'Выход из системы',
                    'path' => 'quit',
                    'key' => 'quit',
                ],
            ],
            'contents' => [
                [
                    'icon' => '',
                    'title' => 'Новости',
                    'path' => 'news',
                    'key' => 'news',
                ],
                [
                    'icon' => '',
                    'title' => 'Альбомы',
                    'path' => 'albums',
                    'key' => 'albums',
                ],
                [
                    'icon' => '',
                    'title' => 'Страницы',
                    'path' => 'pages',
                    'key' => 'pages',
                ],
                [
                    'icon' => '',
                    'title' => 'Объявления',
                    'path' => 'events',
                    'key' => 'events',

                ],
                [
                    'icon' => '',
                    'title' => 'Вопросы и ответы',
                    'path' => 'questions',
                    'key' => 'questions',

                ],
                [
                    'icon' => '',
                    'title' => 'Распорядок дня',
                    'path' => 'schedules',
                    'key' => 'schedules',

                ],
                [
                    'icon' => '',
                    'title' => 'Выпускники',
                    'path' => 'graduates',
                    'key' => 'graduates',
                ],
                [
                    'icon' => '',
                    'title' => 'Формы обратной связи',
                    'path' => 'business_forms',
                    'key' => 'business_forms',
                ],
            ],
            'icon' => $this->features()->pluck('id'),
            'languages' =>  $languages->pluck('id'),
            'locale' => ($languages) ?  $languages->pluck('title', 'code') : new stdClass,
            'text' => FeatureResource::collection(Feature::whereType('text')->get())->collection->map(function ($item) use ($values) {
                $element[] = [
                    'feature_id' => $item->id,
                    'code' => $item->code,
                    'title' => $item->title,
                    'value' =>  optional(optional($values)[optional($item)->code])->value,
                    'all' => $values
                ];
                return $element;
            })->collapse(),
        ];
    }
}
