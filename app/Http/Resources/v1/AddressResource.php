<?php

namespace App\Http\Resources\v1;

use App\Http\Resources\v1\Admin\LocationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $location =  new LocationResource($this->whenLoaded('location'));

        return [
            'id' => $this->id,
            'translations' => $this->translations,
            'name' => $this->name,
            'email' => $this->email,
            'phone_1' => $this->phone_1,
            'phone_2' => $this->phone_2,
            'phone_3' => $this->phone_3,
            'address' => $this->address,
            'lat' => (float) $this->lat,
            'lon' => (float) $this->lon,
            'location_id' => $this->location_id,
            'location' => $location,
            'full_address' => (($location) ? ((optional($location)->parent ? optional(optional($location)->parent)->title . ', ' : '') . optional($location)->title . ', ') : '') . $this->address,
        ];
    }
}
