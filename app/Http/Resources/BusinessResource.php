<?php

namespace App\Http\Resources;

use App\Http\Resources\v1\AddressResource;
use App\Http\Resources\v1\Admin\FormResource;
use App\Http\Resources\v1\Admin\LanguageResource;
use App\Http\Resources\v1\Admin\MediaResource;
use App\Http\Resources\v1\Admin\SeasonResource;
use App\Models\Form;
use App\Models\Project\Status;
use Illuminate\Http\Resources\Json\JsonResource;

class BusinessResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'title' => $this->title,
            'code' => $this->code,
            'logo' => $this->logo,
            'addresses' => AddressResource::collection($this->addresses),
            'seasons' => SeasonResource::collection($this->seasons),
            'languages' => LanguageResource::collection($this->languages)->collection->pluck('title', 'code'),
            'description' => $this->description,
            'course_count' => $this->course_count,
            'is_active' => $this->is_active,
            'is_production' => $this->is_production,
            'features' => $this->features,
            'translations' => $this->translations,
            'values' => $this->values->load('feature'),
            'events' => $this->posts()->home('event', 3)->get(),
            'news' => $this->posts()->home('new', 3)->get(),
            'pages' => $this->posts()->home('page', 20)->get(),

            'schedules' => [],

            'weeks' =>    Status::whereType('week')->with(['posts' => function ($query) {
                $query->whereHas('rubrics', function ($query) {
                    $query->whereCode('schedule');
                })
                    ->whereBusinessId($this->id)
                    ->orderBy('time_from')
                    ;
            }])
                ->whereHas('posts', function ($query) {
                    $query->whereHas('rubrics', function ($query) {
                        $query->whereCode('schedule');
                    })
                        ->whereBusinessId($this->id);
                })
                ->get(),

            'form' => new FormResource(Form::whereCode('feedback')->limit(1)->with('fields')->first()),
            'media' => MediaResource::collection($this->whenLoaded('media')),
        ];
    }
}
