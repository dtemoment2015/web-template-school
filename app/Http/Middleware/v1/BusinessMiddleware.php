<?php

namespace App\Http\Middleware\v1;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\App;
use App\Models\Business;
use App\Models\Business\Domain;
use Illuminate\Support\Str;
use Closure;
use Illuminate\Http\Request;

class BusinessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $domainName  =  (string) $request->header('Client-Host', null);
        $lang = $request->getLocale();
        $business =  Cache::rememberForever('business_domain_' . $domainName . $lang, function () use ($domainName) {
            if ($domain = Domain::whereName($domainName)
                ->limit(1)
                ->whereHas('business', function ($query) {
                    $query
                        ->whereIsActive(true)
                        ->whereIsProduction(true);
                })
                ->first()
            ) {
                return $domain->business;
            } else {
                return Business::whereName(
                    (string) Str::remove('.edutoday.uz', $domainName)
                )
                    ->whereIsActive(true)
                    ->whereIsProduction(true)
                    ->limit(1)
                    ->firstOrFail();
            }
        });

        $request->request->add(
            [
                '_business' =>   $business,
                '_lang' =>   $lang,
            ]
        );

        return $next($request);
    }
}
