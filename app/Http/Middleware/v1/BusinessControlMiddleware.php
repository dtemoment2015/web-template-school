<?php

namespace App\Http\Middleware\v1;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class BusinessControlMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        $bid =  $request->header('bid', null);


        $_user = $request->user();

        if (optional($request)->json_) {
            foreach ($request->json_ as $jsonKey => $json) {
                $request->request->add([(string) $jsonKey => json_decode($json, true)]);
            }
        }

        $_business = $_user->businesses()
            ->whereId($bid)
            ->limit(1)
            ->firstOrFail();

        if (in_array($request->method(), ['PUT', 'POST', 'DELETE'])) {
            foreach (config('translatable.locales') as $lang) {
                Cache::forget('business_' . $_business->id . $lang);
                Cache::forget('business_languages' . $_business->id);
                Cache::forget('business_domain_' . $_business->name.'.edutoday.uz'. $lang);
            }
            foreach ($_business->domains as $domain) {
                foreach (config('translatable.locales') as $lang) {
                    Cache::forget('business_domain_' . $domain->name . $lang);
                }
            }
        }

        $request->request->add(
            [
                '_user' => $_user,
                '_business' =>  $_business,
            ]
        );

        return $next($request);
    }
}
