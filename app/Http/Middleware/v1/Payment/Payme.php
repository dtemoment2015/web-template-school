<?php

namespace App\Http\Middleware\v1\Payment;

use Closure;
use Illuminate\Http\Request;

use App\Api\Paycom\PaycomException;


class Payme
{
    protected $config = [
        'key' => 'VcJjR@a1pS?8urSzQ4@JXIbsNR4u?i05AyEc',
        'login' => 'Paycom',
    ];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $headers['Authorization'] = request()->header('Authorization');
        if (
            !$headers || !isset($headers['Authorization']) ||
            !preg_match('/^\s*Basic\s+(\S+)\s*$/i', $headers['Authorization'], $matches) ||
            base64_decode($matches[1]) != $this->config['login'] . ":" . $this->config['key']
        ) {
            $exception =  new PaycomException(
                request('id'),
                'Insufficient privilege to perform this method. (Auth)',
                PaycomException::ERROR_INSUFFICIENT_PRIVILEGE
            );
            $result  = $exception->send();
            header('Content-Type: application/json');
            echo json_encode($result->original);
            exit;
        }

        return $next($request);
    }
}
