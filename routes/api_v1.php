<?php

use App\Http\Controllers\v1\Promo\IndexController as PromoIndexController;
use App\Http\Controllers\v1\Admin\AuthController;
use App\Http\Controllers\v1\Admin\Business\BusinessAddressController;
use App\Http\Controllers\v1\Admin\Business\BusinessCustomerController;
use App\Http\Controllers\v1\Admin\Business\BusinessDomainController;
use App\Http\Controllers\v1\Admin\Business\BusinessFeedbackController;
use App\Http\Controllers\v1\Admin\Business\BusinessFormController;
use App\Http\Controllers\v1\Admin\Business\BusinessGroupController;
use App\Http\Controllers\v1\Admin\Business\BusinessGroupCustomerController;
use App\Http\Controllers\v1\Admin\Business\BusinessGroupGrouplistController;
use App\Http\Controllers\v1\Admin\Business\BusinessGrouplistController;
use App\Http\Controllers\v1\Admin\Business\BusinessGroupStudentController;
use App\Http\Controllers\v1\Admin\Business\BusinessLanguageController;
use App\Http\Controllers\v1\Admin\Business\BusinessMediaController;
use App\Http\Controllers\v1\Admin\Business\BusinessPostContentController;
use App\Http\Controllers\v1\Admin\Business\BusinessPostController;
use App\Http\Controllers\v1\Admin\Business\BusinessSeasonController;
use App\Http\Controllers\v1\Admin\Business\BusinessStudentController;
use App\Http\Controllers\v1\Admin\Business\BusinessStatController;
use App\Http\Controllers\v1\Admin\Business\BusinessTransactionController;
use App\Http\Controllers\v1\Admin\Business\IndexController;
use App\Http\Controllers\v1\Admin\BusinessController;
use App\Http\Controllers\v1\Admin\CustomerController;
use App\Http\Controllers\v1\Admin\DomainController;
use App\Http\Controllers\v1\Admin\FeatureController;
use App\Http\Controllers\v1\Admin\FieldController;
use App\Http\Controllers\v1\Admin\FormController;
use App\Http\Controllers\v1\Admin\FormFieldController;
use App\Http\Controllers\v1\Admin\InstallController;
use App\Http\Controllers\v1\Admin\LanguageController;
use App\Http\Controllers\v1\Admin\MediaController;
use App\Http\Controllers\v1\Admin\PaymentController;
use App\Http\Controllers\v1\Admin\PositionController;
use App\Http\Controllers\v1\Admin\PostController;
use App\Http\Controllers\v1\Admin\ProfileController;
use App\Http\Controllers\v1\Admin\RubricController;
use App\Http\Controllers\v1\Admin\StatController;
use App\Http\Controllers\v1\Admin\StatusController;
use App\Http\Controllers\v1\Admin\StudentController;
use App\Http\Controllers\v1\Admin\SystemController;
use App\Http\Controllers\v1\Admin\TariffController;
use App\Http\Controllers\v1\Admin\TextController;
use App\Http\Controllers\v1\Admin\TransactionController;
use App\Http\Controllers\v1\Client\ClientCustomerController;
use App\Http\Controllers\v1\Client\ClientFeedbackController;
use App\Http\Controllers\v1\Client\ClientFilterController;
use App\Http\Controllers\v1\Client\ClientGroupController;
use App\Http\Controllers\v1\Client\ClientPostController;
use App\Http\Controllers\v1\Client\IndexController as ClientIndexController;
use App\Http\Controllers\v1\Client\TextController as ClientTextController;
use App\Http\Controllers\v1\Payment\PaymeController;
use Illuminate\Support\Facades\Route;


Route::middleware(['localization'])->group(

   function () {
      Route::group(
         [
            'prefix' => 'admin'
         ],
         function () {
            Route::get('system', [SystemController::class, 'index']);
            Route::group(
               [
                  'middleware' => 'auth:customer'
               ],
               function () {
                  Route::apiResource('profile', ProfileController::class);
                  Route::apiResources(
                     [
                        'stats' => StatController::class,
                        'domains' => DomainController::class,
                        'posts' => PostController::class,
                        'businesses' => BusinessController::class,
                        'customers' => CustomerController::class,
                        'features' => FeatureController::class,
                        'languages' => LanguageController::class,
                        'payments' => PaymentController::class,
                        'positions' => PositionController::class,
                        'rubrics' => RubricController::class,
                        'statuses' => StatusController::class,
                        'students' => StudentController::class,
                        'tariffs' => TariffController::class,
                        'texts' => TextController::class,
                        'media' => MediaController::class,
                        'transactions' => TransactionController::class,
                        'forms' => FormController::class,
                        'forms.fields' => FormFieldController::class,
                        'fields' => FieldController::class,
                     ]
                  );
                  Route::apiResource('init', IndexController::class);

                  Route::group(
                     [
                        'prefix' => 'business',
                        'middleware' => 'business_control'
                     ],
                     function () {
                        Route::apiResource('install', InstallController::class);
                        Route::apiResources(
                           [
                              'languages' => BusinessLanguageController::class,
                              'addresses' => BusinessAddressController::class,
                              'customers' => BusinessCustomerController::class,
                              'domains' => BusinessDomainController::class,
                              'media' => BusinessMediaController::class,
                              'groups' => BusinessGroupController::class,
                              'groups.grouplists' => BusinessGroupGrouplistController::class,
                              'groups.students' => BusinessGroupStudentController::class,
                              'groups.customers' => BusinessGroupCustomerController::class,
                              'grouplists' => BusinessGrouplistController::class,
                              'posts' => BusinessPostController::class,
                              'posts.contents' => BusinessPostContentController::class,
                              'seasons' => BusinessSeasonController::class,
                              'students' => BusinessStudentController::class,
                              'transactions' => BusinessTransactionController::class,
                              'feedback' => BusinessFeedbackController::class,
                              'stats' => BusinessStatController::class,
                              'forms' => BusinessFormController::class,
                           ]
                        );
                     }
                  );
               }
            );
            Route::group(['prefix' => 'auth'], function () {
               Route::controller(AuthController::class)->group(
                  function () {
                     Route::post('code', 'getCode')->middleware('throttle:6,1');
                     Route::post('login', 'doLogIn');
                  }
               );
            });
         }
      );



      Route::get('client/test', [ClientIndexController::class, 'test']);

      Route::group(
         [
            'prefix' => 'client',
            'middleware' => 'business'
         ],
         function () {
            Route::post('/', [ClientIndexController::class, 'index']);
            Route::get('languages', [ClientIndexController::class, 'languages']);

            Route::apiResources(
               [
                  'texts' => ClientTextController::class,
                  'filters' => ClientFilterController::class,
                  'posts' => ClientPostController::class,
                  'feedback' => ClientFeedbackController::class,
                  'customers' => ClientCustomerController::class,
                  'groups' => ClientGroupController::class,
               ]
            );
         }
      );

      Route::group(
         [
            'prefix' => 'payment',
         ],
         function () {
            Route::post('payme', [PaymeController::class, 'index'])->middleware('payme');
         }
      );

      Route::resource('texts', ClientTextController::class);
      Route::get('promo', [PromoIndexController::class, 'index']);
      Route::get('promo/{code}', [PromoIndexController::class, 'show']);
   }
);
