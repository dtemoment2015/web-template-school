<?php

use App\Http\Controllers\v1\Order\IndexController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Yandex\Translate\Translator;
use Yandex\Translate\Exception;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/order/export-pdf', [IndexController::class, 'exportPdf']);

// Route::get('/', function () {
//     // $translator = new Translator('222');
//     // $translation = $translator->translate('Hello world', 'en-ru');
//     // return 1;
// });

// Route::get('/linkstorage', function () {
//     Artisan::call('storage:link');
// });