<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tariff_translations', function (Blueprint $table) {
            $table->bigInteger('tariff_id')->index()->unsigned();
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->string('locale', 16)->index();
            $table->unique(['tariff_id', 'locale']);
            $table->foreign('tariff_id')->references('id')->on('tariffs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tariff_translations');
    }
};
