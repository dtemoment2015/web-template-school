<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grouplist_post', function (Blueprint $table) {
            $table->bigInteger('grouplist_id')->index()->unsigned();
            $table->bigInteger('post_id')->index()->unsigned();
            $table->unique(['grouplist_id', 'post_id']);
            $table->foreign('grouplist_id')->references('id')->on('grouplists')->onDelete('cascade');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grouplist_post');
    }
};
