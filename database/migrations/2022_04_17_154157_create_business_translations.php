<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_translations', function (Blueprint $table) {
            $table->bigInteger('business_id')->index()->unsigned();
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->string('locale', 16)->index();
            $table->unique(['business_id', 'locale']);
            $table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_translations');
    }
};
