<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('album_grouplist', function (Blueprint $table) {
            $table->bigInteger('album_id')->index()->unsigned();
            $table->bigInteger('grouplist_id')->index()->unsigned();
            $table->unique(['album_id', 'grouplist_id']);
            $table->foreign('album_id')->references('id')->on('albums')->onDelete('cascade');
            $table->foreign('grouplist_id')->references('id')->on('grouplists')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('album_grouplist');
    }
};
