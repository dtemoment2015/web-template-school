<?php

use App\Enums\PaymentType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('code')->index()->nullable();
            $table->string('logo')->nullable();
            $table->string('currency')->default('UZS');
            $table->boolean('is_active')->default(0);
            $table->boolean('is_online')->default(0);
            $table->string('type', 32)->nullable();
            $table->float('transaction_price', 16)->default(0);
            $table->float('transaction_price_percent', 16)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
};
