<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //выпускной
        Schema::create('graduates', function (Blueprint $table) {
            $table->id();
            $table->string('code')->index()->nullable();
            $table->bigInteger('business_id')->unsigned()->index();
            $table->bigInteger('grouplist_id')->index()->unsigned();
            $table->bigInteger('season_id')->index()->unsigned();
            $table->timestamps();
            $table->foreign('grouplist_id')->references('id')->on('grouplists')->onDelete('cascade');
            $table->foreign('season_id')->references('id')->on('seasons')->onUpdate('cascade');
            $table->foreign('business_id')->references('id')->on('businesses')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('graduates');
    }
};
