<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('value_translations', function (Blueprint $table) {
            $table->bigInteger('value_id')->index()->unsigned();
            $table->text('title')->nullable();
            $table->string('locale', 16)->index();
            $table->unique(['value_id', 'locale']);
            $table->foreign('value_id')->references('id')->on('values')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('value_translations');
    }
};
