<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position_translations', function (Blueprint $table) {
            $table->bigInteger('position_id')->index()->unsigned();
            $table->text('title')->nullable();
            $table->string('locale', 16)->index();
            $table->unique(['position_id', 'locale']);
            $table->foreign('position_id')->references('id')->on('positions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_translations');
    }
};
