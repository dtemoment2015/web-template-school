<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->text('llc')->nullable();
            $table->text('address')->nullable();
            $table->text('phone')->nullable();
            $table->text('number')->nullable();
            $table->text('bank_address')->nullable();
            $table->text('mfo')->nullable();
            $table->text('inn')->nullable();
            $table->text('oked')->nullable();
            $table->text('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('llc');
            $table->dropColumn('address');
            $table->dropColumn('phone');
            $table->dropColumn('number');
            $table->dropColumn('bank_address');
            $table->dropColumn('mfo');
            $table->dropColumn('inn');
            $table->dropColumn('oked');
            $table->dropColumn('name');
        });
    }
};
