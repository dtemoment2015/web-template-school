<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('season_translations', function (Blueprint $table) {
            $table->bigInteger('season_id')->index()->unsigned();
            $table->text('title')->nullable();
            $table->string('locale', 16)->index();
            $table->unique(['season_id', 'locale']);
            $table->foreign('season_id')->references('id')->on('seasons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('season_translations');
    }
};
