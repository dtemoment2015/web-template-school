<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('album_post', function (Blueprint $table) {
            $table->bigInteger('album_id')->index()->unsigned();
            $table->bigInteger('post_id')->index()->unsigned();
            $table->unique(['album_id', 'post_id']);
            $table->foreign('album_id')->references('id')->on('albums')->onDelete('cascade');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('album_post');
    }
};
