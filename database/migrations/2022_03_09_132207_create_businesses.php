<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->string('code')->index()->nullable();
            $table->string('logo')->nullable();
            $table->tinyInteger('course_count')->default(11);
            $table->boolean('is_active')->default(1); //CLIENT SET

            $table->boolean('done_dns')->default(0); //CLIENT SET
            $table->boolean('done_ssl')->default(0); //CLIENT SET
            $table->boolean('done_domain')->default(0); //CLIENT SET
            $table->boolean('done_alias')->default(0); //CLIENT SET
            $table->boolean('done_redirect')->default(0); //CLIENT SET

            $table->boolean('is_production')->default(0); //SYSTEM SET
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
};
