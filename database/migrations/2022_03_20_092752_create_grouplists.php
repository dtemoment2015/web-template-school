<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grouplists', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('group_id')->index()->unsigned();
            $table->bigInteger('season_id')->index()->unsigned();
            $table->string('course')->nullable();
            $table->string('title')->nullable();
            $table->unique(['group_id', 'season_id']);
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grouplists');
    }
};
