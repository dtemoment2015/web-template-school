<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->boolean('is_paid')->default(0);
            $table->boolean('is_active')->default(0);
            $table->bigInteger('business_id')->unsigned()->index();
            $table->bigInteger('status_id')->unsigned()->index();
            $table->bigInteger('payment_id')->unsigned()->index();
            $table->bigInteger('tarilff_id')->unsigned()->index();
            $table->timestamp('paid_at')->nullable();
            $table->timestamp('cancel_at')->nullable();
            $table->timestamp('from_at')->nullable();
            $table->timestamp('till_at')->nullable();
            $table->string('currency');
            $table->float('price', 16); //price for use 1 time
            $table->timestamps();
            $table->foreign('business_id')->references('id')->on('businesses')->onUpdate('cascade');
            $table->foreign('status_id')->references('id')->on('statuses')->onUpdate('cascade');
            $table->foreign('tarilff_id')->references('id')->on('tariffs')->onUpdate('cascade');
            $table->foreign('payment_id')->references('id')->on('payments')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
