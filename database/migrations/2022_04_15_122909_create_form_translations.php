<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_translations', function (Blueprint $table) {
            $table->bigInteger('form_id')->index()->unsigned();
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->string('locale', 16)->index();
            $table->unique(['form_id', 'locale']);
            $table->foreign('form_id')->references('id')->on('forms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_translations');
    }
};
